#include "gradient.h"
#include "rgba.h"

#include <iostream>
#include <fstream> // to read the files

Gradient::Gradient()
{}

Gradient::Gradient(const char* filename)
{   // construct colour gradient from file:
    std::ifstream infile(filename);

    float point, red, green, blue;
    // make sure control points are empty:
    m_control_points.clear();
    m_control_values.clear();
    m_colour_gradient.clear();
    while (infile >> point >> red >> green >> blue)
    {
        this->addPoint(point, red, green, blue);
    }
}

void Gradient::addPoint(float point, float red, float green, float blue)
{
    m_control_points.push_back(point);
    m_control_values.emplace_back(red, green, blue);

    m_colour_gradient.emplace(std::piecewise_construct, std::forward_as_tuple(point), std::forward_as_tuple(red, green, blue) );
}

void Gradient::removePoint(float point)
{
    qDebug() << "removing point ...";
    this->debug();
    auto it=m_colour_gradient.find(point);
    if(it != m_colour_gradient.end()) m_colour_gradient.erase(it);
    else qDebug() << "not found " << point;
}

void Gradient::debug()
{
    qDebug() << "?Gradient-point-colour-pairs:\n";
    for (auto it : m_colour_gradient)
    {
        qDebug() << " ? " << it.first << " -> " << QString::fromStdString(it.second.getRGBAString()) << "\n";
    }
}
