#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QGLWidget>
//#include "camera.h"
#include "Camera/Arcball.h"
#include "dataset.h"
#include <QOpenGLShaderProgram>
//#include "MatrixStack.h"
#include <QElapsedTimer>
#include <sstream> // for writing out to .ppm file

struct KDTree
{
    KDTree()
        : u1(0), v1(0), w1(0), u2(0), v2(0), w2(0),
          tn_left_ptr(nullptr), tn_right_ptr(nullptr), tn_depth(1)
    {}
    int tn_bounding_volume;
    // this is what defines the bounding box:
    int u1, v1, w1,        // corner 1
        u2, v2, w2;        // corner 2
    KDTree* tn_parent;     // left child
    KDTree* tn_left_ptr;   // left child
    KDTree* tn_right_ptr;  // right child
    int tn_depth;          // what depth of the tree node is in ...
                           // ... (used to determine cutting plane axis)
    int tn_plane;          // chosen splitting plane
    int tn_split_axis;     // chosen splitting axis
    bool tn_is_leaf;
    char tn_side;
};

class GLWidget : public QGLWidget
{   // delete [] pVolume;
    Q_OBJECT
public:
    /* implement virtual functions in QGLWidget ...
     * ... to perform typical OpenGL functions */

    // constructor:
    explicit GLWidget(QWidget *parent = nullptr);

    // destructor:
    ~GLWidget() override;

    // set up OpenGL resources resources and state
    void initializeGL() override;

    // render the OpenGL scene:
    void paintGL() override;
    void frame();
    void computeFrame();
    RGBA transfer(float value, Gradient col_gradient, Alpha opacities);
    float transfer_alpha(float value, Alpha opacities);
    void transfer_function_to_array(float return_array[]);
    void cameraSetTopView(HMatrix& rot);
    void cameraSetCornerView(HMatrix& rot);

    int m_frame;
    GLuint m_camera_data_tex = 4;
    GLuint m_boundingbox_data_tex = 5;
    GLuint m_samplevolumeID = 10;
    GLuint m_sampleRGBAvolumeID = 11;
    GLuint m_gradient_data_sampler = 9;
    GLuint m_transfer_function_sampler = 8;
    GLuint m_fbo = 2;
    GLuint m_gpu_output = 2;

    GLint64 frame_nanosecs = 0;
    GLint64 sum_frame_nanosecs = 0;
    float   avg_frame_ms = 0;
    int     frame_count = 0;

    unsigned char* ppm_pixels;

    // set up OpenGL viewport, projection; executed the first time and after resizing
    void resizeGL(int w, int h) override;
    void displayVersionOpenGL();
    void displayMaxWorkGroupCount();
    void displayMaxWorkGroupSize();
    void displayMaxWorkGroupInvocations();


    // -------------------------- methods dealing with data -------------------------- //
//    bool loadVolumeData(const char* filename);
    bool loadVolumeData(QString filename);
//    int parseDataSizeFromFilename(const char* filename);
    int parseDataSizeFromFilename(QString filename);
    bool loadVolumeData();
    void updateTestDataset();

    void generateVolumePlanes();
    void generateDiscreteNestedCubes();
    void generateSmoothNestedCubes();
    int getCubeDistance(int dimension, int coordinate);
    void precomputeGradient();
    bool isEdgeVoxel(size_t index, unsigned short dimx, unsigned short dimy, unsigned short dimz);

//    void keyPressEvent(QKeyEvent* event) override;
    void mousePressEvent (QMouseEvent* event ) override;
    void mouseMoveEvent(QMouseEvent*) override;
    void mouseReleaseEvent(QMouseEvent* event) override;
    void keyPressEvent(QKeyEvent* event) override;

    void debugCamera(HMatrix rot);

    void cameraTexCreate();
    void auxiliaryDataBind();
    void boundingboxDataBind(float* bounding_box_data, int bounding_box_count);

    void uint8TransferFunctionBind(GLuint texture_index);
    void floatTransferFunctionBind(GLuint texture_index);

    void uint8GradientBind(GLuint texture_index);
    void floatGradientBind(GLuint texture_index);

    void framebufferBind(GLuint texture_index);

    void uint8VolumeBind(GLuint texture_index);
    void floatVolumeBind(GLuint texture_index);

    void floatRGBVolumeBind(GLuint texture_index);

    void buildBinaryVolume(float threshold);
    void buildSummedVolumeTable();
    float sumVolumeTable(int i, int j, int k);

    float computeBV(KDTree* node);
    float splitCost(KDTree* node);

    float getNumNonEmptyCells(int u1, int v1, int w1, int u2, int v2, int w2);
    void destroyTheTree(KDTree* Node);

    int getIdx(int i, int j, int k);

    KDTree m_kdtree_root;
    int    m_current_node = 0;
    void  initTree();
    void  splitNode(KDTree* node);
    void  kdTreeBuild();
    void  kdTreePrint();
    int   getPlaneAxis(KDTree* node);

    void kdTreeVolumise(KDTree* node, float* volumisedKD, float* bounding_boxes);
    void  kdTreePrintNode(KDTree* node);
    void  boundVolumeBoxMinMax(int bounding_box[6],
                               int& x_min, int& x_max,
                               int& y_min, int& y_max,
                               int& z_min, int& z_max);

    void  boundVolumeBoxSummedVT(int bounding_box[6],
                                 int& x_min, int& x_max,
                                 int& y_min, int& y_max,
                                 int& z_min, int& z_max);

    float getNumR(int u1, int u2,
                int v1, int v2,
                int w1, int w2);

    void countLeafNodes(KDTree* node);

    void colourVoxels();
    void colourVoxels(float* custom_dataset);

    KDTree* generateLeftBV(KDTree* node, int plane, int x_min, int y_min, int z_min,
                                                    int x_max, int y_max, int z_max);
    KDTree* generateRightBV(KDTree* node, int plane, int x_min, int y_min, int z_min,
                                                     int x_max, int y_max, int z_max);
    void  generateBVs(KDTree* node, int plane);

    bool  isDivisable(KDTree* node);
    int   selectSplittingPlane(KDTree* node, int dimx, int dimy, int dimz, float& min_cost, int& plane_axis);
    int   computeVolumeBV(KDTree* node);

    void CreatePPMImage(bool binary, const char* filename,
                        int width, int height,
                        unsigned int max_colour,  unsigned char* ppm_pixels);

    void compare2ppm(const char *filename1, const char *filename2,
                     const char* out_filename);

    void trigger_ESS_state_change(int status);
    void trigger_update_bounding_box(int status);
    void trigger_precompute_colours(int status);
    void trigger_lerp(int status);
    void trigger_log(int status);

    void triggerUpdate();
    void writeVolumeToFile(int size, GLubyte* volume);
    float logScaleAlpha(float value);


    QOpenGLShaderProgram program;
    int tex_w, tex_h;
    BallData m_arcball;
    Dataset m_dataset;
    // volume data for the kd tree optimisation:
    float* pVolume1; // 0 for no data 1 for data
    float* pVolume2; //
    int    m_bounding_box_count = 1;

    Gradient m_undo_col_gradient;
    Alpha    m_undo_col_alphas;

    bool m_mouse_pressed;
    bool m_colours_are_ready = false;
    bool m_update_colours_on_cpu = false; // for evaluation
    bool m_ess_enabled = false; // for evaluation
    bool m_BBupdate_enabled = false;
    bool m_precompute_colours = false;
    bool m_lerp = false; // for evaluation
    bool m_log = false;

    bool  m_take_photo = false; // for testing
    bool  m_pause = false;
    float m_show_box = 1.f;
    float m_rotating_light = 0.f;

    // timing frames per second:
    int m_frames = 0;
    double m_fps = 1.0;
    double m_milisec = 0.0;
    QElapsedTimer* frame_time;
    QElapsedTimer* ms_per_frame;

    int m_chosen_dataset = 0;

    int m_specular_on = 1;
    int m_diffuse_on  = 1;
    int m_ambient_on  = 1;

    float m_clamp_start = 1.f;
    float m_clamp_end   = 1.f;

    float m_step_size   = 0.01f;
    float m_max_steps   = 174.f;


private slots:
    void slotGradient(Gradient newGradient);
    void slotColourUpdatePacket(RGBA);
    void slotAddNewPoint(RGBA newPoint);
    void slotRemovePoint(float oldPoint);
    void slotAddNewPoint2(RGBA newPoint);
    void slotRemovePoint2(float oldPoint);
    void slotAddAlphaPoint(Alpha newPoint);
    void slotRemoveAlphaPoint(float oldPoint);
    void slotAddAlphaPoint2(Alpha newPoint);
    void slotRemoveAlphaPoint2(float oldPoint);

    void updateAngle();

    void updateDatasetFile(QString data_file);
    void slotResetColours();
    void slotResetTF();

    void slotUndoAlphas(Alpha oldAlpha);
    void slotUndoColour(Gradient oldGradient);

    void slotLightingInfo(int light_info);

    void slotClampStart(float clamp_start);
    void slotClampEnd(float clamp_end);
    void slotStepSize(float step_size);
    void slotMaxSteps(float max_steps);

    void slotColourFile(QString colour_file);
    void slotAlphaFile(QString alpha_file);
    void slotDataFile(QString data_file);

signals:
    void signalGradient(Gradient newGradient);
    void signalAlpha(Alpha newAlpha);

//    void signalGradient2(Gradient newGradient);
    void signalAlpha2(Alpha newAlpha);
    void signalGradient2(Gradient newGradient);

    void signalOptimalStep(double optimal_step);
    void signalOptimalMaxSteps(double optimal_max_steps);

    void fpsChanged(int fps);
    void msChanged(int fps);

};

#endif // GLWIDGET_H
