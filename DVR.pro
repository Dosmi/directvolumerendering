QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    alpha.cpp \
    alphaset.cpp \
    dataset.cpp \
    glwidget.cpp \
    gradient.cpp \
    gradientpaint.cpp \
    gradientset.cpp \
    main.cpp \
    mainwindow.cpp \
    rgba.cpp \
    Camera/Arcball.cpp\
    Camera/ArcballAux.cpp\
    Camera/ArcballMath.cpp

HEADERS += \
    alpha.h \
    alphaset.h \
    controlpoint.h \
    dataset.h \
    glwidget.h \
    gradient.h \
    gradientpaint.h \
    gradientset.h \
    mainwindow.h \
    rgba.h \
    transferfunction.h \
    Camera/Arcball.h \
    Camera/ArcballAux.h \
    Camera/ArcballMath.h

FORMS += \
    mainwindow.ui

LIBS += -lOpengl32 -lfreeglut -lglu32 -lglew32

DESTDIR = $$PWD

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    vrc.frag
