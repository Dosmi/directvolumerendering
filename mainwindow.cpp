#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QColorDialog>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    pTimer = new QTimer;
    pTimer->start(10);
    connect(pTimer, SIGNAL(timeout()),  ui->Display, SLOT(updateAngle()));
    connect(ui->Display, &GLWidget::fpsChanged,
            this, &MainWindow::updateFPS);
    connect(ui->Display, &GLWidget::msChanged,
            this, &MainWindow::updateMS);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::updateFPS(int fps)
{
    // std::
    QString text_fps = QString::number(fps);
//	this->fpsLabel->setText("Frames per second: " + text_fps);
    qDebug() << "fps " + text_fps;
}

void MainWindow::updateMS(int ms)
{
    QString text_fps = QString::number(ms);
//	this->fpsLabel->setText("Frames per second: " + text_fps);
    qDebug() << "ms " + text_fps;
}

void MainWindow::slotOptimalStep(double optimal_step)
{
    ui->sbox_step_size->setValue(optimal_step);
    ui->label_optstep->setNum(optimal_step);
    ui->label_voxsize->setNum(optimal_step*2);

}

void MainWindow::slotOptimalMaxSteps(double optimal_max_steps)
{
    ui->sbox_max_steps->setValue(int(optimal_max_steps));
}


void MainWindow::on_red_dbl_spinbox_valueChanged(double value)
{
    m_col_gradient.m_colour_gradient.find(float(m_active_value))->second.m_red = float(value);

    RGBA col_packet(float(m_active_value), float(value), RED);
    emit(signalColourUpdatePacket(col_packet));

    ui->widget->repaint();
}

void MainWindow::on_green_dbl_spinbox_valueChanged(double value)
{
    m_col_gradient.m_colour_gradient.find(float(m_active_value))->second.m_green = float(value);

    RGBA col_packet(float(m_active_value), float(value), GREEN);
    emit(signalColourUpdatePacket(col_packet));

    ui->widget->repaint();
}

void MainWindow::on_blue_dbl_spinbox_valueChanged(double value)
{
    m_col_gradient.m_colour_gradient.find(float(m_active_value))->second.m_blue = float(value);

    RGBA col_packet(float(m_active_value), float(value), BLUE);
    emit(signalColourUpdatePacket(col_packet));

    ui->widget->repaint();
}

void MainWindow::on_set_colour_clicked()
{
    QColor colour = QColorDialog::getColor(Qt::white, this, "Pick Colour");
    if(colour.isValid())
    {
        this->ui->red_dbl_spinbox->setValue(colour.red());
        this->ui->green_dbl_spinbox->setValue(colour.green());
        this->ui->blue_dbl_spinbox->setValue(colour.blue());
    }
}

void MainWindow::slotGradient(Gradient newGradient)
{
    m_col_gradient = newGradient;
}

void MainWindow::slotAlpha(Alpha newAlpha)
{
    m_col_alphas = newAlpha;
}

void MainWindow::slotActiveValue(float newActiveValue)
{
    m_active_value = newActiveValue;
}

void MainWindow::slotSetDialog(float newDialogValue)
{
    ui->ct_number->display(double(newDialogValue));
}

void MainWindow::slotSetRedSpinbox(RGBA newRedValue)
{
    ui->red_dbl_spinbox->setValue(double(newRedValue.m_red));
}

void MainWindow::slotSetGreenSpinbox(RGBA newGreenValue)
{
    ui->green_dbl_spinbox->setValue(double(newGreenValue.m_green));
}

void MainWindow::slotSetBlueSpinbox(RGBA newBlueValue)
{
    ui->blue_dbl_spinbox->setValue(double(newBlueValue.m_blue));
}

void MainWindow::slotLockInput()
{
    ui->red_dbl_spinbox->setEnabled(false);
    ui->blue_dbl_spinbox->setEnabled(false);
    ui->green_dbl_spinbox->setEnabled(false);

    ui->set_colour->setEnabled(false);

    ui->ct_number->setEnabled(false);

    ui->label->setEnabled(false);
    ui->red_label->setEnabled(false);
    ui->blue_label->setEnabled(false);
    ui->green_label->setEnabled(false);
}

void MainWindow::slotUnlockInput()
{
    ui->red_dbl_spinbox->setEnabled(true);
    ui->blue_dbl_spinbox->setEnabled(true);
    ui->green_dbl_spinbox->setEnabled(true);

    ui->set_colour->setEnabled(true);

    ui->ct_number->setEnabled(true);

    ui->label->setEnabled(true);
    ui->red_label->setEnabled(true);
    ui->blue_label->setEnabled(true);
    ui->green_label->setEnabled(true);
}

void MainWindow::on_undo_alpha_clicked()
{
    ui->alpha_set->undoAlpha();
}

void MainWindow::on_undo_colour_clicked()
{
    ui->widget->undoColour();
}

void MainWindow::on_specular_light_cbox_stateChanged(int arg1)
{
    int light_value = 1000;
    light_value += ui->ambient_light_cbox->isChecked() * 100;
    light_value += ui->diffuse_light_cbox->isChecked() * 10;
    light_value += ui->specular_light_cbox->isChecked();

    ui->widget->sendLightInfo(light_value);
}

void MainWindow::on_diffuse_light_cbox_stateChanged(int arg1)
{
    int light_value = 1000;
    light_value += ui->ambient_light_cbox->isChecked() * 100;
    light_value += ui->diffuse_light_cbox->isChecked() * 10;
    light_value += ui->specular_light_cbox->isChecked();

    ui->widget->sendLightInfo(light_value);
}

void MainWindow::on_ambient_light_cbox_stateChanged(int arg1)
{
    int light_value = 1000;
    light_value += ui->ambient_light_cbox->isChecked() * 100;
    light_value += ui->diffuse_light_cbox->isChecked() * 10;
    light_value += ui->specular_light_cbox->isChecked();

    ui->widget->sendLightInfo(light_value);
}

void MainWindow::on_clamp_noise_start_valueChanged(double arg1)
{
    ui->widget->sendClampNoiseStart(float(arg1));
}

void MainWindow::on_clamp_noise_end_valueChanged(double arg1)
{
    ui->widget->sendClampNoiseEnd(float(arg1));
}

void MainWindow::on_sbox_step_size_valueChanged(double arg1)
{
    ui->widget->sendStepSize(float(arg1));
}

void MainWindow::on_sbox_max_steps_valueChanged(int arg1)
{
    ui->widget->sendMaxSteps(float(arg1));
}

void MainWindow::on_load_colours_clicked()
{
    QDir dir;
    if(!QDir("transfer-functions").exists()) QDir().mkdir("transfer-functions");
    QString PATH = dir.absolutePath()+"/transfer-functions";
    QString file_name = QFileDialog::getOpenFileName(this,"Open a colour file",PATH);
    if(file_name != "") ui->widget->sendColourFile(file_name);
}

void MainWindow::on_load_alphas_clicked()
{
    QDir dir;
    if(!QDir("transfer-functions").exists()) QDir().mkdir("transfer-functions");
    QString PATH = dir.absolutePath()+"/transfer-functions";
    QString file_name = QFileDialog::getOpenFileName(this,"Open an opacity file",PATH);

    if(file_name != "") ui->widget->sendAlphaFile(file_name);
}

void MainWindow::on_btn_update_colours_clicked()
{
    ui->Display->triggerUpdate();
}

void MainWindow::on_toggle_ess_stateChanged(int arg1)
{
     ui->Display->trigger_ESS_state_change(arg1);
}

void MainWindow::on_toggle_updateBB_stateChanged(int arg1)
{
     ui->Display->trigger_update_bounding_box(arg1);
}

void MainWindow::on_toggle_precompute_stateChanged(int arg1)
{
    ui->Display->trigger_precompute_colours(arg1);
}

void MainWindow::on_toggle_lerp_stateChanged(int arg1)
{
    ui->Display->trigger_lerp(arg1);
}

void MainWindow::on_toggle_log_stateChanged(int arg1)
{
    ui->Display->trigger_log(arg1);
}

void MainWindow::on_button_change_data_clicked()
{
    QDir dir;
    if(!QDir("data").exists()) QDir().mkdir("data");
    QString PATH = dir.absolutePath()+"/data";
    QString file_name = QFileDialog::getOpenFileName(this,"Open data file",PATH);

    if(file_name != "") ui->widget->sendDataFile(file_name);
}

void MainWindow::on_comboBox_currentIndexChanged(int index)
{
    ui->Display->m_chosen_dataset = index;
    ui->Display->updateTestDataset();
}
