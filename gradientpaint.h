#ifndef GRADIENTPAINT_H
#define GRADIENTPAINT_H

#include <QWidget>
#include "gradient.h"
#include "alpha.h"

class GradientPaint : public QWidget
{
    Q_OBJECT
public:
    explicit GradientPaint(QWidget *parent = nullptr);
    void paintEvent( QPaintEvent * );
//    void mouseDoubleClickEvent ( QMouseEvent * event );

    Gradient m_col_gradient;
    Alpha    m_col_alphas;

    float m_stretch;


signals:
    void signalGradient(Gradient newGradient);
    void signalAlpha(Alpha newAlpha);
public slots:
    void slotGradient(Gradient newGradient);
    void slotAlpha(Alpha newAlpha);
    void slotAlpha2(Alpha newAlpha);
};

#endif // GRADIENTPAINT_H
