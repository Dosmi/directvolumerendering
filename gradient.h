#ifndef GRADIENT_H
#define GRADIENT_H

#include "rgba.h"
#include <vector>
#include <map>
#include <qDebug>
#include <qString>

class Gradient
{
public:
    Gradient();
    Gradient(const char* filename);
    Gradient(short point, RGBA colour);

    void debug();

    void addPoint(float point, RGBA& colour);
    void addPoint(float point, float red, float green, float blue);
    void removePoint(float point);

    std::vector<float> m_control_points;
    std::vector<RGBA>  m_control_values;

    std::map<float, RGBA> m_colour_gradient;
};

#endif // GRADIENT_H
