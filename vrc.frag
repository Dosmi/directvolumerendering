#version 430
layout(local_size_x = 1, local_size_y = 1) in;
//layout(r8ui, binding = 3) uniform readonly uimage3D textureID;
layout(r32f, binding = 4) uniform readonly image1D cameraDATA;
layout(binding = 5) uniform sampler1D bounding_boxes;
layout(binding = 8) uniform  sampler1D  transfer_function_sampler;
layout(binding = 9) uniform  sampler3D gradient;
layout(binding = 10) uniform  sampler3D float_volume_sampler;
layout(binding = 11) uniform  sampler3D float_rgb_volume_sampler;
// output:
layout(rgba32f, binding = 2) uniform image2D img_output;

float tempArray[100];

// how many steps to perform, in the bounding box hit test:
#define RAYMARCH_LIMIT 50
#define DIST 256.
// epsilon setting - how close to the bounding box does the ray have to get ...
// ... to count it as 'hit the box'
#define EPSILON 0.001
// cube (half) dimension, since box is centered around the origin ...
// ... CUBE_DIST_TO_CENTER is the distance of each face to the center
#define CUBE_DIST_TO_CENTER 0.5f

// get distance from arbitrary position to box ...
// ... centered at the origin
float getCentredBoxDist(vec3 position, float width, float length, float height)
{
    float dx = abs(position.x) - width;
    float dy = abs(position.y) - length;
    float dz = abs(position.z) - height;

    dx = clamp(dx, 0, dx);
    dy = clamp(dy, 0, dy);
    dz = clamp(dz, 0, dz);

    float distance = sqrt(dx*dx + dy*dy + dz*dz);
    return distance;
}

// get distance from arbitrary position to box ...
// ... centered at arbitrary location
float getAnyBoxDist(vec3 position, float width, float length, float height,
                    float center_u, float center_v, float center_w)
{
    float dx = abs(position.x - center_u) - width/2;
    float dy = abs(position.y - center_v) - length/2;
    float dz = abs(position.z - center_w) - height/2;

    dx = clamp(dx, 0, dx);
    dy = clamp(dy, 0, dy);
    dz = clamp(dz, 0, dz);

    float distance = sqrt(dx*dx + dy*dy + dz*dz);
    return distance;
}

// get distances of N number of bounding boxes
// (for empty-space-skipping)
float getBoundingBoxesDist(vec3 position, float width, float length, float height,
                           int num_of_bounding_boxes)
{
    float distance_to_bbox = 999.0;
    bool hit_bounding_box = false;

    float u1, v1, w1, u2, v2, w2;
    float tu1, tv1, tw1, tu2, tv2, tw2;
    float cube_dists[10];
    int idx = 0;

    for(int i = 0; i < num_of_bounding_boxes*6; i=i+6)
    {
        // get boundaries of the bounding box
        u1 = texelFetch(bounding_boxes, i+1,0).r;
        v1 = texelFetch(bounding_boxes, i  ,0).r;
        w1 = texelFetch(bounding_boxes, i+2,0).r;
        u2 = texelFetch(bounding_boxes, i+4,0).r;
        v2 = texelFetch(bounding_boxes, i+3,0).r;
        w2 = texelFetch(bounding_boxes, i+5,0).r;

        // compute bounding box centres in all axis:
        float uc = (u2 - u1) / 2 + u1;
        float vc = (v2 - v1) / 2 + v1;
        float wc = (w2 - w1) / 2 + w1;

        // get cube dimensions (since CUBE_DIST_TO_CENTER is distance from the center, ...
        // ... it to get the true cube dimension, it is multiplied by 2)
        float cube_dim = CUBE_DIST_TO_CENTER * 2;
        // assuming the data resolution is a cube (hence res_y = res_x...)
        // (if it is not, when loading the data, the data resolution ...
        //  ... is made to be bound by a cube box anyway, and can be optimised ...
        //  ... by having empty-space-skipping)
        float data_res_x = int(imageLoad(cameraDATA, 26).r);
        float data_res_y = data_res_x;
        float data_res_z = data_res_x;

        // compute bounding box centre coordinates:
        float center_u = (cube_dim/data_res_x) * uc - cube_dim/2;
        float center_v = (cube_dim/data_res_y) * vc - cube_dim/2;
        float center_w = (cube_dim/data_res_z) * wc - cube_dim/2;

        // compute dimensions of the bounding box:
        float len_u = cube_dim/data_res_x * (u2 - u1);
        float len_v = cube_dim/data_res_y * (v2 - v1);
        float len_w = cube_dim/data_res_z * (w2 - w1);

        cube_dists[idx] = getAnyBoxDist(position, len_u, len_v, len_w,
                                        center_u, center_v, center_w);
        idx+=1;
    }

    // get the minimum distance from all the bounding boxes and return that:
    for (int i = 0; i < idx; i++)
    {
        if(cube_dists[i] < distance_to_bbox)
        {
            distance_to_bbox = cube_dists[i];
        }
    }
    return distance_to_bbox;
}

float getDistanceToBoundingBox(vec3 camera_pos, vec3 ray_direction,
                               int num_of_bounding_boxes)
{
    float distance = 0.0;
    vec3 cube = vec3(CUBE_DIST_TO_CENTER, CUBE_DIST_TO_CENTER, CUBE_DIST_TO_CENTER);
    // march for predefined number of steps:
    for (int i = 0; i < RAYMARCH_LIMIT; i++)
    {
        vec3 ray_position = camera_pos + ray_direction * distance;
        float distance_to_object = getBoundingBoxesDist(ray_position, cube.x, cube.y, cube.z,
                                                        num_of_bounding_boxes);
        distance += distance_to_object;

        // if distance is more than the set limit, and is very small, count it as a hit:
        if((distance > DIST) || (distance_to_object < EPSILON)) return distance;
    }
    return distance;
}

float diffuse_clamp(vec3 vector1, vec3 vector2)
{
  float dot = dot(vector1, vector2);
  if ( dot > 0.0 )
  {
      return dot;
  }
  else
  {
      return 0.0;
  }
}

vec3 add_local_illumination(vec3 diffuse_col, float opacity, int shine,
                            vec3 normal, vec3 light_dir, vec3 camera_dir,
                            float specular_on, float diffuse_on, float ambient_on)
{
    vec3 sum = vec3(0.0, 0.0, 0.0);
    vec3 ambient  = vec3(0.0, 0.0, 0.0);
    vec3 diffuse  = vec3(0.0, 0.0, 0.0);
    vec3 specular = vec3(0.0, 0.0, 0.0);

    vec3 light_colour = vec3(1.0, 1.0, 1.0);
    vec3 ambient_colour = vec3(0.5, 0.5, 0.5);
    vec3 specularColor = vec3(1.0);

    float directionality = 0.f;

    // AMBIENT LIGHT:
    // (constant colour)
    ambient += light_colour * ambient_colour;

    // DIFFUSE LIGHT:
    int num_lights = 1;

    for (int i = 0; i < num_lights; i++)
    {
      directionality = diffuse_clamp(light_dir, normal);
      diffuse += directionality * light_colour;
    }

    // SPECULAR LIGHT:
    vec3 direction_to_reflected_ray = 2*dot(light_dir, normal)*normal-light_dir;

    vec3 h_vec = camera_dir + 1 / length(camera_dir+1);
    float dot_prod = 0.0;
    for (int i = 0; i < shine; i++)
    {
        dot_prod += dot(direction_to_reflected_ray, camera_dir);
    }
    // specular lighting - if camera direction and reflected ray direction match, ...
    // ... exponentiate by power of 'shine', else 0.0:
    float spec = pow(max(dot(camera_dir, direction_to_reflected_ray), 0.0), shine);
    // (multiply by 10.f to make the specular highlights more visible)
    specular = 10.f * spec * light_colour;
    // it turned out to be faster to not have branches when computing the lighting ...
    // ... therefore each term is computed anyway and multiplied by '_on':
    return vec3(ambient_on * ambient + diffuse_on * diffuse + specular_on * specular) * diffuse_col;
}

float rand1(vec2 co)
{
    // Pseudorandom function generator, proposed by:
    //    W. J. J. Rey. On generating random numbers, with help of y = [(a + x)sin(bx)]
    //   mod 1. presented at the 22nd European Meeting of Statisticians and the 7th Vilnius
    //   Conference on Probability Theory and Mathematical Statistics, August 1998.
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}



vec4 volume_ray_marching(vec3 camera_pos,
                         vec3 light_pos,
                         vec3 ray_direction,
                         float distance,
                         int volume_dimx, int volume_dimy, int volume_dimz,
                         float ambient_on, float diffuse_on, float specular_on,
                         float noise_clamp_start, float noise_clamp_end,
                         int max_steps, float step_dist)//, int hit)
{
    float value = 0;
    int resx = volume_dimx / 2;
    int resy = volume_dimy / 2;
    int resz = volume_dimz / 2;

    float voxel_dim = CUBE_DIST_TO_CENTER / volume_dimx;
    float voxels_per_step = step_dist / voxel_dim;

    int step_count = 0;
    int step_increment = 1;

    vec4 final_colour = vec4(0.0, 0.0, 0.0, 0.0);
    float red, green, blue, alpha = 0.0;

    vec3 ray_position;

    float sample_cube_x;
    float sample_cube_y;
    float sample_cube_z;

    vec3 sample_index;
    vec4 value_rgba;

    vec4 grad;
    vec3 N, L, C;

    while ((final_colour.a < 0.95) && (step_count < max_steps))
    {
        // compute current ray position
        ray_position = camera_pos + ray_direction * distance;
        // get vector to index the dataset:
        sample_cube_x = (resx - (0.0 - ray_position.y)/(CUBE_DIST_TO_CENTER/resx));
        sample_cube_y = (resy - (0.0 - ray_position.x)/(CUBE_DIST_TO_CENTER/resy));
        sample_cube_z = (resz - (0.0 - ray_position.z)/(CUBE_DIST_TO_CENTER/resz));

        // check if the ray position has left the bounding box coordinate:
        if ((sample_cube_x > resx*2+1) || (sample_cube_y > resy*2+1) || (sample_cube_z > resz*2+1)
           || (sample_cube_x < -1) || (sample_cube_y < -1) || (sample_cube_z < -1))
        {
            break;
        }
        // get a vector to then index the volume with:
        sample_index = vec3(sample_cube_x/volume_dimx,
                            sample_cube_y/volume_dimy,
                            sample_cube_z/volume_dimz);

        // load data value from the volume:
        value = float(texture(float_volume_sampler, sample_index ).r);
        value_rgba = texture(transfer_function_sampler, value/255.0 ).rgba;

        // GPU interpolates values and then assigns colour
        // normalise the voxel opacity - hence multiplication by 'voxels_per_step' ...
        // ... and division by 'float(volume_dimx)/64.f' (otherwise datasets with ...
        // ... higher resolution will become more opaque [note, 64.f choice is arbitrary])
        alpha = (1.0f - exp(-0.5f * value_rgba.a * voxels_per_step))/(float(volume_dimx)/64.f);
        red   = value_rgba.r * alpha;
        green = value_rgba.g * alpha;
        blue  = value_rgba.b * alpha;

        // Normal vector:
        grad = texture(gradient, sample_index).rgba;
        N = normalize(grad.rgb);// / grad.a;
        // Light direction (from current ray position)
        L = normalize(light_pos - ray_position);
        // Camera direction (from current ray position)
        C = normalize(camera_pos - ray_position);
        // add illumination to the colour:
        final_colour.rgb += add_local_illumination(vec3(red, green, blue),
                                                 alpha, 1000,  N,L,C,
                                                 ambient_on, diffuse_on, specular_on);
        final_colour.a   += alpha;

        distance += step_dist * clamp(rand1(vec2(sample_cube_x, value)), noise_clamp_start, noise_clamp_end);
        step_count += step_increment;
    }
    return final_colour / 255.0; // divide by 255 since colour falls in range [0,1]
}

vec4 volume_precomputed(vec3 camera_pos,
                                vec3 light_pos,
                                vec3 ray_direction,
                                float distance,
                                int volume_dimx, int volume_dimy, int volume_dimz,
                                float ambient_on, float diffuse_on, float specular_on,
                                float noise_clamp_start, float noise_clamp_end,
                                int max_steps, float step_dist)
{
    float value = 0;
    vec4 cpu_rgba = vec4(0.0);
    int resx = volume_dimx / 2;
    int resy = volume_dimy / 2;
    int resz = volume_dimz / 2;

    float voxel_dim = CUBE_DIST_TO_CENTER / volume_dimx;
    float voxels_per_step = step_dist / voxel_dim;

    int step_count = 0;
    int step_increment = 1;

    vec4 final_colour = vec4(0.0, 0.0, 0.0, 0.0);
    float red, green, blue, alpha = 0.0;

    vec4 grad;
    vec3 N, L, C;

    while ((final_colour.a < 0.95) && (step_count < max_steps))
    {
        // compute current ray position:
        vec3 ray_position = camera_pos + ray_direction * distance;

        float sample_cube_x = (resx - (0.0 - ray_position.y)/(CUBE_DIST_TO_CENTER/resx));
        float sample_cube_y = (resy - (0.0 - ray_position.x)/(CUBE_DIST_TO_CENTER/resy));
        float sample_cube_z = (resz - (0.0 - ray_position.z)/(CUBE_DIST_TO_CENTER/resz));

        // check if the ray position has left the bounding box coordinate:
        if ((sample_cube_x > resx*2+1) || (sample_cube_y > resy*2+1) || (sample_cube_z > resz*2+1)
           || (sample_cube_x < -1) || (sample_cube_y < -1) || (sample_cube_z < -1))
        {
            break;
        }
        // get a vector to then index the volume with:
        vec3 sample_index = vec3(sample_cube_x/volume_dimx,
                                      sample_cube_y/volume_dimy,
                                      sample_cube_z/volume_dimz);
        // sample the pre-computed colour at the voxel:
        cpu_rgba = texture(float_rgb_volume_sampler, sample_index ).rgba;
        alpha = (1.0f - exp(-0.5f * cpu_rgba.a * voxels_per_step))/(float(volume_dimx)/64.f);
        red   = cpu_rgba.r * alpha;
        green = cpu_rgba.g * alpha;
        blue  = cpu_rgba.b * alpha;

        // Normal vector:
        grad = texture(gradient, sample_index).rgba;
        N = normalize(grad.rgb);// / grad.a;
        // Light direction (from current ray position)
        L = normalize(light_pos - ray_position);
        // Camera direction (from current ray position)
        C = normalize(camera_pos - ray_position);
        // add lighting to the colour:
        final_colour.rgb += add_local_illumination(vec3(red, green, blue), alpha, 1000,  N,L,C,
                                                 ambient_on, diffuse_on, specular_on);
        final_colour.a   += alpha;
        // take a step  (note, can add some random distance to step, to get rid of the ray marching artifact)
        distance += step_dist * clamp(rand1(vec2(sample_cube_x, value)), noise_clamp_start, noise_clamp_end);
        step_count += step_increment;
    }
    return final_colour / 255.0; // divide by 255 since colour falls in range [0,1]
}

void main()
{
    // output image resolution:
    float resx = 512.0;
    float resy = 512.0;

    resx = imageLoad(cameraDATA, 24).r;
    resy = imageLoad(cameraDATA, 25).r;

    // set variables to values that were sent from CPU:
    int volume_dimx = int(imageLoad(cameraDATA, 26).r);
    int volume_dimy = int(imageLoad(cameraDATA, 27).r);
    int volume_dimz = int(imageLoad(cameraDATA, 28).r);

    float ambient_on  = imageLoad(cameraDATA, 30).r;
    float diffuse_on  = imageLoad(cameraDATA, 31).r;
    float specular_on = imageLoad(cameraDATA, 32).r;

    float noise_clamp_start = imageLoad(cameraDATA, 33).r;
    float noise_clamp_end   = imageLoad(cameraDATA, 34).r;

    float step_dist         = imageLoad(cameraDATA, 35).r;
    int max_steps           = int(imageLoad(cameraDATA, 36).r);

    float colours_precomputed = int(imageLoad(cameraDATA, 37).r);

    // use 0, 0 as the central coordinate reference point:
    float view_plane_pixel_x = ((float(gl_GlobalInvocationID.x) - trunc(resx/2.0)) / (resx * 1.0));
    float view_plane_pixel_y = ((float(gl_GlobalInvocationID.y) - trunc(resy/2.0)) / (resy * 1.0));

    // get time-dependent variable sent from CPU:
    // ... for the rotating light
    ivec3 time_index   = ivec3(0, 0, 0);
    uint time = int(imageLoad(cameraDATA, 29).r);

    // load in the camera (eye) position (x,y,z):
    float campos_x     = imageLoad(cameraDATA, 0).r;
    float campos_y     = imageLoad(cameraDATA, 1).r;
    float campos_z     = imageLoad(cameraDATA, 2).r;

    // load in the lookat coordinates (x,y,z):
    float lookat_x     = imageLoad(cameraDATA, 3).r;
    float lookat_y     = imageLoad(cameraDATA, 4).r;
    float lookat_z     = imageLoad(cameraDATA, 5).r;

    // flag variables for demonstration (turn on/off different rendering modes)
    int num_of_bounding_boxes = int(imageLoad(cameraDATA, 22).r);
    float rotating_light = imageLoad(cameraDATA, 23).r;

    // load in the current rotation matrix
    // (which is obtained from mouse movements, ...
    //  ... changing the arcball camera parameters, ...
    //  ... where this matrix is obtained from the method ...
    //  'Qt_ToMatrix', which displays quaternion rotation in matrix form)
    mat4 rotation_matrix = mat4(imageLoad(cameraDATA, 6).r,  imageLoad(cameraDATA, 7).r,  imageLoad(cameraDATA, 8).r,  imageLoad(cameraDATA, 9).r,
                                imageLoad(cameraDATA, 10).r,  imageLoad(cameraDATA, 11).r,  imageLoad(cameraDATA, 12).r,  imageLoad(cameraDATA, 13).r,
                                imageLoad(cameraDATA, 14).r,  imageLoad(cameraDATA, 15).r,  imageLoad(cameraDATA, 16).r,  imageLoad(cameraDATA, 17).r,
                                imageLoad(cameraDATA, 18).r,  imageLoad(cameraDATA, 19).r,  imageLoad(cameraDATA, 20).r,  imageLoad(cameraDATA, 21).r);

    // save original look at point (x, y, z) as vector
    vec3 lookat = vec3(lookat_x, lookat_y, lookat_z);

    // get new camera position ...
    // ... by multiplying old camera position ...
    // ... with the rotation matrix:
    vec3 campos = (rotation_matrix*vec4(campos_x, campos_y, campos_z, 1.0)).xyz;

    // ------------------ Setting up the camera coordinate frame ------------------ //
    vec3 forward_direction = normalize(lookat-campos);
    // instead of using static right vector for rotations ...
    // (which alone causes one rotation axis locking up) ...
    // ... we rotate the 'right' vector together with the camera:
    vec3 right_direction = vec3(-1.0, 0.0, campos_z);
    right_direction      = (rotation_matrix*vec4(right_direction, 1.0)).xyz;

    // compute 'up direction' as cross product beween the right and forward directions, ...
    // ... note, that 'right direction' is then recomputed
    vec3 up_direction    = normalize(cross(right_direction, forward_direction));
    right_direction      = normalize(cross(campos, up_direction));

    float zoom = 0.9;

    vec3 camera = campos + zoom*forward_direction;

    // ------------------ End of setting up the camera coordinate frame ------------------ //

    vec3 view_plane_pos = camera + view_plane_pixel_x * right_direction + view_plane_pixel_y * up_direction;
    vec3 ray_direction = (view_plane_pos - campos);

    float distance_to_volume = getDistanceToBoundingBox(camera, ray_direction,
                                                        num_of_bounding_boxes);
    vec4 pixel;


    if (distance_to_volume > 50) // if bounding cube was not hit, ...
    {
        // ... colour dark grey (background colour)
        pixel = vec4(0.2, 0.2, 0.2, 1.0);
    }

    else // if hit the cube:
    {
        if (colours_precomputed < 0.5f)
        {
            if (rotating_light > 0.5f)
            {
                // rotating light around the center:
                vec3 light_pos = vec3(5*cos(time/10), 1.0, 5*sin(time/10));
                pixel = volume_ray_marching(camera, light_pos, ray_direction, distance_to_volume,
                                                   volume_dimx, volume_dimy, volume_dimz,
                                                   ambient_on, diffuse_on, specular_on,
                                                   noise_clamp_start, noise_clamp_end,
                                                   max_steps, step_dist);
            }
            else
            {
                // camera is the light source:
                vec3 light_pos = camera;
                pixel = volume_ray_marching(camera, light_pos, ray_direction, distance_to_volume,
                                                   volume_dimx, volume_dimy, volume_dimz,
                                                   ambient_on, diffuse_on, specular_on,
                                                   noise_clamp_start, noise_clamp_end,
                                                   max_steps, step_dist);
            }

        }
        else // if precomputed colours:
        {
            // camera is the light source:
            vec3 light_pos = camera;
            pixel = volume_precomputed(camera, light_pos, ray_direction, distance_to_volume,
                                       volume_dimx, volume_dimy, volume_dimz,
                                       ambient_on, diffuse_on, specular_on,
                                       noise_clamp_start, noise_clamp_end,
                                       max_steps, step_dist);
        }

    }

  // global index of work item, ...
  // ... where x and y components correspond to the
  // ... pixel coordinates within the view plane
  ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);

  // write a single texel into an image:
  // given an index (defined above), store a pixel colour ...
  // ... in the output 2D texture
  imageStore(img_output, pixel_coords, pixel);
}

