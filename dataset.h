#ifndef DATALOADER_H
#define DATALOADER_H

#include <cstdint>
#include <memory>
#include "gradient.h"
#include "gradientpaint.h"
#include "alpha.h"
#include "rgba.h"

enum type {uint8, uint16, int8, int16, float16, float32};

class Dataset
{
public:
    Dataset();
    ~Dataset();
    Dataset(const char *filename);
    Dataset(const char *filename, std::size_t voxel_len,
            unsigned short dimx, unsigned short dimy, unsigned short dimz);
    void changeAllData(const char *filename, std::size_t voxel_len,
                       unsigned short dimx, unsigned short dimy, unsigned short dimz);
    void changeAllData2(QString filename, std::size_t voxel_len,
                       unsigned short dimx, unsigned short dimy, unsigned short dimz);

    void setMin(char32_t value);
    void setMax(char32_t value);

    void setGradient(Gradient grad );
    void setAlpha(Alpha    alpha);

    void repaintColourUI();

    uint8_t getMax();
    uint8_t getMin();

    Gradient m_col_gradient;
    Alpha    m_col_alphas;

    GradientPaint m_painter;

    uint8_t   m_ui8min,  m_ui8max;
    uint16_t  m_ui16min, m_ui16max;
    int8_t    m_i8min,   m_i8max;
    int16_t   m_i16min,  m_i16max;
    char16_t  m_f16min,  m_f16max;
    char32_t  m_f32min,  m_f32max;

    // dimensions of data:
    unsigned short m_dimx, m_dimy, m_dimz;

    float* pfloatVolume;     // default volume data storage - converted to float type
    float* pfloatRGBVolume;  // optional volume data storage - precomputed colours
    float* pGradient;        // gradient storage, for local illumination
//    float* m_bounding_boxes; // bounding box storage, for empty space skipping
//    RGBA*  m_normals;

//    const char*    m_filename;
    QString        m_filename;
    std::size_t    m_voxel_len;

private:
    type m_data_type;
};

#endif // DATALOADER_H
