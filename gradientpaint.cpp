#include "gradientpaint.h"
#include <QPainter>

#include <iostream>
#include "transferfunction.h"
//#include "colourgradientdialog.h"

#include <QObject>

GradientPaint::GradientPaint(QWidget *parent) : QWidget(parent), m_stretch(2.8f)
{}

//void GradientPaint::mouseDoubleClickEvent (QMouseEvent* event )
//{
//  ColourGradientDialog col_grad_dlog;

//  col_grad_dlog.painter = this;
//  col_grad_dlog.m_col_alphas = m_col_alphas;
//  col_grad_dlog.m_col_gradient = m_col_gradient;

//  emit(signalGradient( m_col_gradient ));
//  emit(signalAlpha(m_col_alphas));

//  col_grad_dlog.setModal(true);
//  col_grad_dlog.exec();

//}

void GradientPaint::paintEvent( QPaintEvent* )
{

  QPainter p( this );
  // no antialiasing, want thin lines between the cell
  p.setRenderHint( QPainter::Antialiasing, true );

  // black thin boundary around the cells
  //  QPen pen( Qt::red );
  //  pen.setWidth(0.);

//  std::cout << "painting the gradient ... ";
  RGBA rect_slice_col(255.f, 255.f, 255.f);

  int local_stretch = int(m_stretch);
  int red, green, blue, alpha;
  for (int i_column = 0 ; i_column < 100*m_stretch; i_column+=local_stretch)
  {
      QRect rect(i_column,1, local_stretch,100);
      // get the colour components (linear interpolation) of a rectangle slice:
      rect_slice_col = transfer(i_column/m_stretch, m_col_gradient, m_col_alphas);

      red = int(rect_slice_col.m_red  );
      green = int(rect_slice_col.m_green);
      blue = int(rect_slice_col.m_blue );
      alpha = int(rect_slice_col.m_alpha);

      QColor c = QColor(red, green, blue, alpha);

      // fill with color for the pixel
      p.fillRect(rect, QBrush(c));
  }
}


// slots for receiving data sent from other widgets:
void GradientPaint::slotGradient(Gradient newGradient)
{
    m_col_gradient = newGradient;
//    update();
    repaint();
}

void GradientPaint::slotAlpha(Alpha newAlpha)
{
    m_col_alphas = newAlpha;
    repaint();
}

void GradientPaint::slotAlpha2(Alpha newAlpha)
{
    m_col_alphas = newAlpha;
    repaint();
}
