#ifndef ALPHA_H
#define ALPHA_H

#include <vector>
#include <map>

#include <QDebug>

class Alpha
{
public:
    Alpha();
    Alpha(const char* filename);
    void addAlphaPoint(float point, float alpha_value);
    void removeAlphaPoint(float point);
    void debug();
    float logScaleAlpha(float value);

    std::vector<float> m_control_points;
    std::vector<float> m_control_opacities;

    std::map<float,float> m_alpha;

    float m_x, m_y;



};

#endif // ALPHA_H
