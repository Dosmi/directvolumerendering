#include "dataset.h"
#include "gradient.h"
#include "rgba.h"

Dataset::Dataset()
{}

Dataset::~Dataset()
{
    delete pfloatVolume;
    delete pfloatRGBVolume;
    delete pGradient;
//    delete m_bounding_boxes;
//    delete m_normals;
}

Dataset::Dataset(const char *filename)
    : m_dimx(0), m_dimy(0), m_dimz(0),
      m_filename(filename)
{}

Dataset::Dataset(const char *filename, std::size_t voxel_len,
        unsigned short dimx, unsigned short dimy, unsigned short dimz)
    : m_dimx(dimx), m_dimy(dimy), m_dimz(dimz),
      m_filename(filename), m_voxel_len(voxel_len)
{}

void Dataset::changeAllData(const char *filename, std::size_t voxel_len,
                            unsigned short dimx, unsigned short dimy, unsigned short dimz)
{
    m_dimx = dimx;
    m_dimy = dimy;
    m_dimz = dimz;

    m_filename = filename;
    m_voxel_len = voxel_len;

//    delete pfloatVolume;
//    delete pfloatRGBVolume;
//    delete pGradient;
//    delete m_bounding_boxes;
//    delete m_normals;
}

void Dataset::changeAllData2(QString filename, std::size_t voxel_len,
                            unsigned short dimx, unsigned short dimy, unsigned short dimz)
{
    m_dimx = dimx;
    m_dimy = dimy;
    m_dimz = dimz;

    m_filename = filename.toStdString().c_str();
    m_voxel_len = voxel_len;

    delete pfloatVolume;
    delete pfloatRGBVolume;
    delete pGradient;
//    delete m_bounding_boxes;
//    delete m_normals;
}

void Dataset::setGradient(Gradient grad)
{
    m_col_gradient = grad;
}

void Dataset::setAlpha(Alpha alpha)
{
    m_col_alphas = alpha;
}

void Dataset::repaintColourUI()
{
    m_painter.m_col_gradient = m_col_gradient;
    m_painter.update();
}


