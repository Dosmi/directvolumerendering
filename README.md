# DirectVolumeRendering

Repository for the source code of the Direct Volume Rendering project, written in C++, using OpenGL using the ray-casting approach, while (re)sampling from 3D raw binary files.<br/>

Controls:

Opacity function control points:<br/>
<ul>
- Move with left mouse button, by clicking on individual control point<br/>
- Delete with right-click, by clicking on individual control point<br/>
- Add new point - double-left-click on the opacity function
</ul>

RGB colour control points:
to change colour of control point:<br/>
- left-click on control point, then click 'Set Colour', below 'Value'
to delete colour control point:
- right-click on control point

to load new dataset, (.raw file, with _DIMxDIMxDIM_, where DIM is the dimension in the file name (for example, for data file 64 cubed, _64x64x64_ in filename.

Toggle lighting with lighting control boxes 'Ambient' 'Diffuse' 'Specular'

Noise clamp (start and end) add noise to the ray marching sampling to hide sampling artifacts
NOTE if start and end set to 1.00, it is off

Load in demo datasets from choice
- Discrete Nested Cubes 4 nested cubes, with sharp edges between boundaries)
- Smooth Nested Cubes - nested cubes without sharp boundary
- Planes - each layer in x axis have uniform data values

Sampling - by default, the optimal step is chosen (half the voxel size)
use the mouse scroll wheel to change the sampling or max steps fields, alternatively, write the value from keyboard input.
<br/>
Reset alphas/Reset colours - resets opacity/colour settings from the DEFAULT files:<br/>
 "table-opacity.txt" and "table-colour.txt" - make sure these files are in the root directory to begin with

Load alphas/colours - load external opacity/colour files
opacity file format:
<control point>_SPACE_<value>
example:<br/>
0.0 0.0<br/>
10.0 20.0<br/>
50.0 0.0<br/>
70.0 50.0<br/>
91.0 100.<br/>
100.0 0.0<br/>

colour file format:<br/>
\<control point>_SPACE_\<RED value>_SPACE_\<GREEN value>_SPACE_\<BLUE value><br/>

example:<br/>
0.0 0.0 0.0 0.0 <br/>
1.0 255.0 0.0 0.0<br/>
10.0 255.0 0.0 0.0<br/>
90.0 238.0 130.0 238.0<br/>
99.0 255.0 255.0 255.0<br/>


-- SETTINGS BELOW DISPLAY WIDGET --<br/>
-Skip empty spaces - computes and turns on bounding boxes <br/>
-Update Bounding boxes on transfer function change - recomputes bounding boxes every time the opacity values of the transfer function are changed (slow)<br/>
-Interpolate Voxels - interpolates data trilinearly if on, and nearest neighbour if off (blocky)<br/>
-Logarithmic Alpha - map the opacity points via formula exp(value/22.0), else if OFF, just gets 'value' for the opacity component.<br/>
-Precompute Voxel colours - assign colours for each voxel independently on the CPU, no more GPU data value interpolation, just interpolates the colours<br/>
-Update Colours - relevant if 'Precompute Voxel Colours' selected ON, updates colours on CPU once the transfer function is updated. Else, if 'Precompute Voxel Colours' is selected, it does not automatically update the colours. <br/>


-- OPTIONS FOR BUILDING THE PROJECT --<br/>

- To install Qt 5.12.6, along with the Qt Creator 4.10 environment used during the development, install Qt from the archive:<br/>
https://download.qt.io/archive/qt/5.12/5.12.6/
(qt-opensource-windows-x86-5.12.6.exe - it is a 3.7GB download).

- The project was built using QtCreator 4 on 64bit Windows , therefore, if that is available, this project can be loaded in by FILE->Open New Project and then selecting DVR.pro
NOTE that in order to work, the GPU needs to support OpenGL versions OpenGL 4.3 (2012) since the compute shaders were introduced since then.<br/>

- For kits, choose Desktop Qt 5.12.6 MinGW 32/64-bit (the project used 64-bit, and the the locations where libraries have to be moved will depend on this).

- Build and Compilation will produce errors that it is not possible to find -lfreeglut and -lglew32 since Qt does not come with these pre-installed, and it can also complain about missing header files. We will add the header files and libraries to the required locations in Qt next.

- The project needs glew32 installed and freeglut 
it was developed with the mingw73_64 software development environment, therefore the following files were added to Qt\5.12.6\mingw73_64\include\GL:
'freeglut.h', 'freeglut_ext.h', 'freeglut_std.h' and 'glut.h'
<br/>
The header files can be found in the root of this project, under `GL/`

- The library files (glew32.lib, glew32s.lib libfreeglut.a), are under `GL/libs` and need to be moved to:<br/>
`C:\Qt\Qt5.12.6\Tools\mingw730_64\x86_64-w64-mingw32\lib`

- Then, once the project is loaded, it needs to be built (Built->Build Project DVR, top menu bar settings) and lauched (Build->Run or simply CTRL+R)

- The project can fail at this point, producing: <br/>
`The program has unexpectedly finished.`<br/>
This can be solved by moving the freeglut.dll and glew32.dll dynamically linked libraries (under `GL/dlls`) to: <br/>
`C:\Qt\Qt5.12.6\5.12.6\mingw73_64\bin`


- The alternative is to build the file in console, using 'qmake' or 'qmake DVR.pro' (which is obtained by installing Qt) in the same folder where DVR.pro is, which then creates a makefile, and when the makefile is ready, typing 'make', which compiles an executable.


For convenience, the executable with all the required libraries, has been included in the repository under the name:
'DVRrelease.zip', which, when downloaded and unzipped, runs when launching DVR.exe (note, this again requires OpenGL version 4.3+ support)

By default, the dataset loaded is the generated 'Discrete Nested Cubes' dataset
