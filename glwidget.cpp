#include <GL/glew.h>
#include "glwidget.h"
#include <QApplication>
#include <GL/gl.h>
#include <GL/glut.h>

#include <QDebug>
#include <iostream>
#include <fstream>
#include <string>

#include <QMouseEvent>
#include "dataset.h"
#include <QOpenGLContext>
#include "mainwindow.h"

#include <QTime>
#include <QRegularExpression>

#define GLM_SWIZZLE
#define LIMIT 100
#define RAYMARCH_LIMIT 50
#define DIST 256.f
#define EPSILON 0.01f
#define BOUNDING_CUBE_DIM 1.f
#define BOUNDING_CUBE_HALFDIM 0.5f
#define SMALLEST_STEP_ALLOWED 0.00001f

//#include "transferfunction.h"

#include <QKeyEvent>

#define GLEW_STATIC

GLWidget::GLWidget(QWidget *parent)
    : QGLWidget(parent),
      m_frame(32),
      program(this),
      m_dataset("Demo_64x64x64_file", 64*64*64, 64, 64, 64)
{   
    m_dataset.setGradient(Gradient("table-colours.txt"));
    m_dataset.setAlpha(Alpha("table-opacity.txt"));
    this->setFocusPolicy(Qt::StrongFocus);
}

GLWidget::~GLWidget()
{
    // delete all textures that were once sent off to GPU
    glBindTexture(GL_TEXTURE_1D, 0);
    glDeleteTextures(1, &m_camera_data_tex);

    glBindTexture(GL_TEXTURE_3D, 0);
    glDeleteTextures(1, &m_samplevolumeID);

    glBindTexture(GL_TEXTURE_3D, 0);
    glDeleteTextures(1, &m_gradient_data_sampler);

    glBindTexture(GL_TEXTURE_1D, 0);
    glDeleteTextures(1, &m_transfer_function_sampler);

    glBindTexture(GL_TEXTURE_2D, 0);
    glDeleteTextures(1, &m_gpu_output);

    delete[] ppm_pixels;
    delete pVolume1;
    delete pVolume2;

    delete ms_per_frame;
    delete frame_time;

    destroyTheTree(&m_kdtree_root);
}

void GLWidget::initializeGL()
{
    // 1. Initialise OpenGL extension wrangler
    // (which loads pointers to OpenGL functions ...
    //  ... since they are dependent on hardware ...
    //  ... as they are implemented by device drivers)
    if (glewInit() != GLEW_OK)
    {
        qDebug() << "Error!";
    }

    // 2. initialise camera parameters and camera:
    m_arcball.start_distance = 2.5f;
    m_arcball.current_distance = 2.5f;

    // initialise arcball camera:
    Ball_Init(&m_arcball);

    // 3. set the colour gradient and opacity values in UI
    // (since they were set in glwidget, we signal the values ...
    //  ... to gradientset and alphaset)
    emit(signalGradient( m_dataset.m_col_gradient ));
    emit(signalAlpha(m_dataset.m_col_alphas));

    unsigned int a;
    glGenBuffers(1, &a);

    // 4. load data to array:
//    this->loadVolumeData(m_dataset.m_filename);
    this->loadVolumeData();

    // 5. pre-compute gradient of data on CPU:
    this->precomputeGradient();

    // 6. initialise a compute shader from file, ...
    // ... compile it and show error messages:
    QOpenGLShader shader(QOpenGLShader::Compute);
    shader.compileSourceFile("vrc.frag");
    if (!shader.isCompiled())
    {
        qDebug() << "@failed compilation with message:";
        qDebug() << "@->" << shader.log();
    }
    program.addShader(&shader);
    program.link();

    // 7. bind the volume as float array
    // (since we index with sampler3D in compute shader)
    this->floatVolumeBind(m_samplevolumeID);

    // pre-compute each voxel colour, is option is toggled:
    if(m_update_colours_on_cpu)
    {
        this->colourVoxels();
        this->floatRGBVolumeBind(m_sampleRGBAvolumeID);
        // reset the flag:
        m_update_colours_on_cpu = false;
    }

    // 8. bind the gradient which has been precomputed:
    this->floatGradientBind(m_gradient_data_sampler);

    ms_per_frame = new QElapsedTimer;
    frame_time = new QElapsedTimer;
    frame_time->start();

    // 9. set the output image resolution ...
    // ... and allocate memory for the output image:
    tex_w = 772; tex_h = 740;
    // allocate  memory once for the output image (for ppm file)
    ppm_pixels = new unsigned char[tex_w*tex_h*3];

    // 10. generate and bind the texture, where gpu output image
    // ... (the pixel colours) will be stored):
    glGenTextures(1, &m_gpu_output);
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, m_gpu_output);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, tex_w, tex_h, 0, GL_RGBA, GL_FLOAT, nullptr);
    glBindImageTexture(2, m_gpu_output, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA32F);

    // 11. generate and bind framebuffer, attaching 'm_gpu_output' image, ...
    // ... which was bound before; framebuffer is a memory buffer ...
    // ... containing data that represents pixel colours of the frame
    glGenFramebuffers(1, &m_fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_gpu_output, 0);

    // 12. set the target of reading operations to be the frame buffer defined above ...
    // ... which reads from image 'm_gpu_output':
    glBindFramebuffer(GL_READ_FRAMEBUFFER, m_fbo);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
}

void GLWidget::computeFrame()
{
    // -------------- creating 1d texture for CAMERA data ------------- //

    // 1. Bind camera data, since it is dynamic and can change frame-to-frame:
    this->auxiliaryDataBind();

    // 2. Bind the transfer function, which is also dynamic and changes:
    this->floatTransferFunctionBind(m_transfer_function_sampler);

    // since precomputing colours takes some time, ...
    // ... to keep the transfer function modification fast, ...
    // ... update precomputed colours only when the user has specified so:
    if(m_update_colours_on_cpu)
    {
        this->colourVoxels();
        this->floatRGBVolumeBind(m_sampleRGBAvolumeID);
        // reset the flag
        m_update_colours_on_cpu = false;
    }

    // 3. Bind the program and dispatch the compute shader job
    program.bind();

    GLuint query = 1;
    glGenQueries(1, &query);
    glBeginQuery(GL_TIME_ELAPSED, query);
        glDispatchCompute(GLuint(tex_w), GLuint(tex_h), 1);
    glEndQuery(GL_TIME_ELAPSED);
    glGetQueryObjecti64v(query, GL_QUERY_RESULT, &frame_nanosecs);
    glDeleteQueries(1, &query);
//    qDebug() << "@Compute Shader nanoseconds taken: " << frame_nanosecs;

    frame_count++;
    sum_frame_nanosecs += frame_nanosecs;
//    qDebug() << frame_count << " = " << frame_nanosecs << "ms";
//    if(frame_count == 10)
//    {
////        m_pause = true;
//        frame_count = 0;

//        qDebug() << "@avg-frame" << (float(sum_frame_nanosecs) / 1000000) / 10;
//        sum_frame_nanosecs = 0;
//    }

    glFinish();
    // make sure writing to image has finished before read
    glMemoryBarrier(GL_TEXTURE_FETCH_BARRIER_BIT);
//    glMemoryBarrier(GL_TEXTURE_UPDATE_BARRIER_BIT);

    // 4. copy one framebuffer to other
    // (the gpu output to framebuffer that is displayed):
    glBlitFramebuffer(
        0, 0, tex_w, tex_h,
        0, 0, tex_w, tex_h,
        GL_COLOR_BUFFER_BIT, GL_NEAREST);

    // if the user pressed 'P', save the framebuffer to .ppm file:
    if(m_take_photo)
    {
        // read framebuffer, save pixel colours:
        glReadPixels(0, 0, tex_w, tex_h, GL_RGB, GL_UNSIGNED_BYTE, ppm_pixels);
        // dynamically create file name:
        QString filename = QDateTime::currentDateTime().toString("yyyy-MM-dd-hh_mm_ss");
        filename += "-lerp-" + QString::number(m_lerp) + "-step-"
                 +QString::number(double(m_step_size)) + "-max-" + QString::number(double(m_max_steps)) + ".ppm";
        const char* file = filename.toStdString().c_str();

        //   with defined pixel values, we can write them to the PPM file:
        CreatePPMImage(/* binary */     true,
                       /* filename*/    file,
                       /* width */      tex_w,
                       /* height */     tex_h,
                       /* max_colour */ 255,
                       /* col buffer */ ppm_pixels);
        // reset the flag:
        m_take_photo = false;
        m_step_size   = 0.01f;
        m_max_steps   = 150.f;
    }
}

void GLWidget::CreatePPMImage(bool binary, const char *filename,
                              int width, int height,
                              unsigned int max_colour,
                              unsigned char* pixels)
{
    FILE *fp = fopen(filename, "wb"); /* b - binary mode */

    if (binary)
    {
        (void) fprintf(fp, "P6\n%d %d\n%d\n", width, height, max_colour);
    }
    else
    {
        (void) fprintf(fp, "P3\n%d %d\n%d\n", width, height, max_colour);
    }

    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
        {
          int cur = 3 * ((height - i - 1) * width + j);
          if(binary)
          { // wtrite pixel values as binary:
            static unsigned char color[3];
            color[0] = pixels[cur];  /* red */
            color[1] = pixels[cur + 1];  /* green */
            color[2] = pixels[cur + 2];  /* blue */
            (void) fwrite(color, 1, 3, fp);
          }
          else
            fprintf(fp, "%3d %3d %3d ", pixels[cur], pixels[cur + 1], pixels[cur + 2]);
        }
        if(!binary) fprintf(fp, "\n");
    }
    qDebug() << "finished writing to file ...";
    (void) fclose(fp);
}

void GLWidget::trigger_ESS_state_change(int status)
{
    m_ess_enabled = status;
    this->floatVolumeBind(m_samplevolumeID);
}

void GLWidget::trigger_update_bounding_box(int status)
{
    m_BBupdate_enabled = status;
}

void GLWidget::trigger_precompute_colours(int status)
{
    m_precompute_colours = status;
    if(!m_colours_are_ready)
    {
        this->colourVoxels();
        this->floatRGBVolumeBind(m_sampleRGBAvolumeID);
    }
}

void GLWidget::trigger_lerp(int status)
{
    m_lerp = status;
    this->floatVolumeBind(m_samplevolumeID);
    this->floatRGBVolumeBind(m_sampleRGBAvolumeID);
}

void GLWidget::trigger_log(int status)
{
    m_log = status;
}

RGBA GLWidget::transfer(float value, Gradient col_gradient, Alpha opacities)
{
    float lerp_red, lerp_green, lerp_blue, lerp_alpha;

    // there are two disjoint arrays:
    // ... col_gradient - containing the control points for the RGB colours, ...
    // ... opacities    - containing the control points for the opacities

    // here, for a given value, we find two neighbours to linearly interpolate between ...
    // ... (for colour and opacity values):
    float lower_col_value = std::prev((col_gradient.m_colour_gradient.lower_bound(value)),1)->first;
    float upper_col_value = col_gradient.m_colour_gradient.lower_bound(value)->first;

    float lower_alpha_value = std::prev((opacities.m_alpha.lower_bound(value)),1)->first;
    float upper_alpha_value = opacities.m_alpha.lower_bound(value)->first;

//    std::cout << "neighbours of " << value << ": " << lower_col_value << ", " << upper_col_value << " -> ";

    RGBA lower_nbor_colour    = std::prev((col_gradient.m_colour_gradient.lower_bound(value)),1)->second;
    RGBA upper_nbor_colour    = col_gradient.m_colour_gradient.lower_bound(value)->second;
    float lower_nbor_alpha    = std::prev((opacities.m_alpha.lower_bound(value)),1)->second;
    float upper_nbor_alpha    = opacities.m_alpha.lower_bound(value)->second;

    float col_weight   = (value - lower_col_value)/(upper_col_value - lower_col_value);
    float alpha_weight = (value - lower_alpha_value)/(upper_alpha_value - lower_alpha_value);

//    std::cout << " weight: " << col_weight << ", starting: " << lower_nbor_colour.getRGBAString() << ",      ";

    lerp_red   = (1-col_weight) * lower_nbor_colour.m_red   + col_weight * upper_nbor_colour.m_red;
    lerp_green = (1-col_weight) * lower_nbor_colour.m_green + col_weight * upper_nbor_colour.m_green;
    lerp_blue  = (1-col_weight) * lower_nbor_colour.m_blue  + col_weight * upper_nbor_colour.m_blue;

    lerp_alpha = (1-alpha_weight) * lower_nbor_alpha + alpha_weight * upper_nbor_alpha;

    // return the full colour, given in RGBA ...
    // ... (with given 'alpha' being in %, hence division by 100 and multiplication by 255)
    float alpha_value = (lerp_alpha/100);// *255; // 2021-05-07
    // if user requested log alpha scale, compute log transparancy:
    if(m_log) alpha_value = (logScaleAlpha(lerp_alpha)/100); // *255; 2021-05-07

    return RGBA(lerp_red, lerp_green, lerp_blue, alpha_value);
}

float GLWidget::transfer_alpha(float value, Alpha opacities)
{
    float lerp_alpha;

    float lower_alpha_value = std::prev((opacities.m_alpha.lower_bound(value)),1)->first;
    float upper_alpha_value = opacities.m_alpha.lower_bound(value)->first;

    float lower_nbor_alpha    = std::prev((opacities.m_alpha.lower_bound(value)),1)->second;
    float upper_nbor_alpha    = opacities.m_alpha.lower_bound(value)->second;

    float alpha_weight = (value - lower_alpha_value)/(upper_alpha_value - lower_alpha_value);

    lerp_alpha = (1-alpha_weight) * lower_nbor_alpha + alpha_weight * upper_nbor_alpha;

    // return the full colour, given in RGBA ...
    // ... (with given 'alpha' being in %, hence division by 100 and multiplication by 255)
    float alpha_value = (lerp_alpha/100); //*255; 2021-05-07
    // if user requested log alpha scale, compute log transparancy:
    if(m_log) alpha_value = (logScaleAlpha(lerp_alpha)/100); //*255; 2021-05-07

    return alpha_value;
}

// create an array with 100 RGBA values ...
// that contains colours for each percentage 1-100%:
// (this is sent to the GPU)
void GLWidget::transfer_function_to_array(float return_array[])
{
    for (unsigned int i = 0; i < 100; i++)
    {
        RGBA slice = transfer(float(i), m_dataset.m_col_gradient, m_dataset.m_col_alphas);
        unsigned int index = i*4;
        return_array[index]   = slice.m_red;
        return_array[index+1] = slice.m_green;
        return_array[index+2] = slice.m_blue;

        return_array[index+3] = slice.m_alpha;
    }
}

// function to set the camera rotation matrix to show the object from top
void GLWidget::cameraSetTopView(HMatrix& rot)
{
    rot[0][0] = 1; rot[0][1] = 0;  rot[0][2] = 0; rot[0][3] = 0;
    rot[1][0] = 0; rot[1][1] = 0;  rot[1][2] = 1; rot[1][3] = 0;
    rot[2][0] = 0; rot[2][1] = -1; rot[2][2] = 0; rot[2][3] = 0;
    rot[3][0] = 0; rot[3][1] = 0;  rot[3][2] = 0; rot[3][3] = 1;
}
// function to set the camera rotation matrix to show the object's corner
void GLWidget::cameraSetCornerView(HMatrix& rot)
{
    rot[0][0] = 0.75f; rot[0][1] = 0.f;    rot[0][2] = -0.6f; rot[0][3] = 0.f;
    rot[1][0] = 0.45f; rot[1][1] = 0.75f;  rot[1][2] = 0.45f; rot[1][3] = 0.f;
    rot[2][0] = 0.55f; rot[2][1] = -0.6f;  rot[2][2] = 0.6f;   rot[2][3] = 0.f;
    rot[3][0] = 0;     rot[3][1] = 0;      rot[3][2] = 0;     rot[3][3] = 1.f;
}

void GLWidget::paintGL()
{
//    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // push camera as first thing on the stack:
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    /*****************************************************************************/
    // apply camera view - sets up gluLookAt:
    gluLookAt(0.0, 0.0, double(m_arcball.current_distance),
              0.0, 0,0,
              0.0, 1.0, 0.0);

    // rotate object using quaternion rotations:
    glMultMatrixf((float*)m_arcball.mNow);
    /*****************************************************************************/


    m_dataset.repaintColourUI();

//    ms_per_frame->start();
//    auto m_StartTime = std::chrono::system_clock::now();
//    bool m_bRunning = true;
//    using namespace std::chrono;
//    steady_clock::time_point t1 = steady_clock::now();
    if(!m_pause) this->computeFrame();
//    this->frameCPU();
//    steady_clock::time_point t2 = steady_clock::now();
//    duration<double> time_span = duration_cast<duration<double>>(t2 - t1);

//    std::chrono::time_point<std::chrono::system_clock> endTime;

//    if(m_bRunning)
//    {
//        endTime = std::chrono::system_clock::now();
//    }

//    qDebug() << "#ms_+" << time_span.count();
//    qDebug() << "@ms+" << std::chrono::duration_cast<std::chrono::milliseconds>(endTime - m_StartTime).count();
//    m_bRunning = false;

//    emit msChanged(int(ms_per_frame->elapsed()));
////    ms_per_frame->start();

//    m_frames++;
//    if (frame_time->elapsed() >= 1000)
//    {
//        m_milisec = double(frame_time->elapsed());
//        m_fps = m_frames / (m_milisec/1000.0);
//        emit fpsChanged(int(m_fps));

//        m_frames = 0;
//        frame_time->start();
//    }

    if(!m_pause) glFlush();

}

void GLWidget::mouseMoveEvent(QMouseEvent* event)
{   // get cursor x and y positions:
    int x = event->x();
    int y = event->y();

    Ball_Mouse(&m_arcball, x, y, QWidget::width(), QWidget::height());
    Ball_Update(&m_arcball);
    updateGL();
}

void GLWidget::mousePressEvent(QMouseEvent* event)
{   // get cursor x and y positions:
    int x = event->x();
    int y = event->y();

    m_arcball.mouse_start_x = x;
    m_arcball.mouse_start_y = y;
    HVect vNow;
    vNow.x = 2.f*(x - 0) / QWidget::width() - 1.f;
    vNow.y = 2.f*(y - 0) / QWidget::height() - 1.f;

    Ball_Mouse(&m_arcball, vNow);

    m_mouse_pressed = true;

    switch (event->buttons())
    {
    case Qt::LeftButton:
        Ball_BeginDrag(&m_arcball, LEFT);
        break;
    case Qt::MiddleButton:
        break;
    case Qt::RightButton:
        Ball_BeginDrag(&m_arcball, RIGHT);
        m_arcball.current_distance = m_arcball.start_distance;
        break;
    default:
        break;
    }
    updateGL();
}

void GLWidget::keyPressEvent(QKeyEvent* event)
{
    switch (event->key())
    {
      case Qt::Key_D:
        if(m_show_box > 0.5f)
        {
            m_show_box = 0.f;
        }

        else if(m_show_box < 0.5f)
        {
            m_show_box = 1.f;
        }
      break;

    case Qt::Key_P:
        m_take_photo = !m_take_photo;
        break;

    case Qt::Key_O:
        m_pause = !m_pause;
        break;

    case Qt::Key_E:
      if(m_rotating_light > 0.5f)
      {
          m_rotating_light = 0.f;
      }

      else if(m_rotating_light < 0.5f)
      {
          m_rotating_light = 1.f;
      }
    break;
    }
    updateGL();
}

void GLWidget::debugCamera(HMatrix rot)
{
    qDebug() << "!camera-transform: \n"
    << "!< (0) [0][0] " << rot[0][0] << " (4) [0][1] " << rot[0][1] << " (8) [0][2] " << rot[0][2] << " (12) [0][3] " << rot[0][3] << ">\n"
    << "!< (1) [1][0] " << rot[1][0] << " (5) [1][1] " << rot[1][1] << " (9) [1][2] " << rot[1][2] << " (13) [1][3] " << rot[1][3] << ">\n"
    << "!< (2) [2][0] " << rot[2][0] << " (6) [2][1]" << rot[2][1] << " (10) [2][2] " << rot[2][2] << " (14) [2][3]" << rot[2][3] << ">\n"
    << "!< (3) [3][0] " << rot[3][0] << " (7) [3][1]" << rot[3][1] << " (11) [3][2] " << rot[3][2] << " (15) [3][3]" << rot[3][3] << ">\n]";
}

void GLWidget::cameraTexCreate()
{
    glGenTextures(1, &m_camera_data_tex);
    glActiveTexture(GL_TEXTURE4);
    glBindTexture(GL_TEXTURE_1D, m_camera_data_tex);
      glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
      glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
      glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//    glTexImage1D(GL_TEXTURE_1D, 0, GL_R32F, data_len, 0, GL_RED, GL_FLOAT, camera_data);
//    glBindImageTexture(4, m_camera_data_tex, 0, GL_FALSE, 0, GL_READ_ONLY, GL_R32F);
}

void GLWidget::auxiliaryDataBind()
{
    // send additional data to GPU (not related to the volume ...
    // ... such as camera position and other UI settings)
    HMatrix rot;
    Qt_ToMatrix(m_arcball.qNow, &rot);

    int data_len = 38;
    // GLuint camera_data_tex = 4;
    GLfloat camera_data[] = {/* camera position */ 0.0, 0.0, m_arcball.current_distance, //m_camera.GetDistance(),
                           /* looking at      */ 0.0, 0.0, 0.0,
                           /* camera rotation */ rot[0][0], rot[0][1], rot[0][2], rot[0][3],
                                                 rot[1][0], rot[1][1], rot[1][2], rot[1][3],
                                                 rot[2][0], rot[2][1], rot[2][2], rot[2][3],
                                                 rot[3][0], rot[3][1], rot[3][2], rot[3][3],
                           /* show cube?      */ float(m_bounding_box_count),
                           /* show voxel?     */ m_rotating_light,

                           /* screen width    */ float(tex_w),
                           /* screen height   */ float(tex_h),

                           /* volume dim x    */ float(m_dataset.m_dimx),
                           /* volume dim y    */ float(m_dataset.m_dimy),
                           /* volume dim z    */ float(m_dataset.m_dimz),

                           /* time component  */ float(m_frame),
                           /* ambient         */ float(m_ambient_on),
                           /* diffuse         */ float(m_diffuse_on),
                           /* specular        */ float(m_specular_on),

                           /* clamp start     */ m_clamp_start,
                           /* clamp end       */ m_clamp_end,

                           /* step size       */ m_step_size,
                           /* max steps       */ m_max_steps,

                           /* precomputed cpu */ 1.f*m_precompute_colours
                          };
    glGenTextures(1, &m_camera_data_tex);
    glActiveTexture(GL_TEXTURE4);
    glBindTexture(GL_TEXTURE_1D, m_camera_data_tex);
      glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
      glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
      glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage1D(GL_TEXTURE_1D, 0, GL_R32F, data_len, 0, GL_RED, GL_FLOAT, camera_data);
    glBindImageTexture(4, m_camera_data_tex, 0, GL_FALSE, 0, GL_READ_ONLY, GL_R32F);
}

void GLWidget::boundingboxDataBind(float* bounding_box_data, int bounding_box_count)
{
    glGenTextures(1, &m_boundingbox_data_tex);
    glActiveTexture(GL_TEXTURE5);
    glBindTexture(GL_TEXTURE_1D, m_boundingbox_data_tex);
      glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
      glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
      glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage1D(GL_TEXTURE_1D, 0, GL_R32F, bounding_box_count*6, 0, GL_RED, GL_FLOAT, bounding_box_data);
    glBindImageTexture(5, m_boundingbox_data_tex, 0, GL_FALSE, 0, GL_READ_ONLY, GL_R32F);
}

void GLWidget::uint8TransferFunctionBind(GLuint texture_index)
{
    // -------------- creating 1d texture for TRANSFER FUNCTION ------------- //
    // need 3 floats for lookat and 3 floats for camera pos:

  //    int tf_data_len = 255*4;
      int tf_data_len = 100*4;

  //    m_dataset.m_col_gradient.debug();

      float colour_transfer[400];
      transfer_function_to_array(colour_transfer);

      glGenTextures(1, &texture_index);
      glActiveTexture(GL_TEXTURE5);
      glBindTexture(GL_TEXTURE_1D, texture_index);
      glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
      glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
      glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      glTexImage1D(GL_TEXTURE_1D, 0, GL_R32F, tf_data_len, 0, GL_RED, GL_FLOAT, colour_transfer);
      glBindImageTexture(5, texture_index, 0, GL_FALSE, 0, GL_READ_ONLY, GL_R32F);
}

void GLWidget::floatTransferFunctionBind(GLuint texture_index)
{
    // START ------------ creating 1d sampler for TRANSFER FUNCTION ------------- //
  //    int tf_data_len = 100*4;

  //    m_dataset.m_col_gradient.debug();

    float colour_transfer[400];
    transfer_function_to_array(colour_transfer);



    glGenTextures(1, &texture_index);
    glActiveTexture(GL_TEXTURE8);
    glBindTexture(GL_TEXTURE_1D, texture_index);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage1D(GL_TEXTURE_1D, 0, GL_RGBA32F, 100, 0, GL_RGBA, GL_FLOAT, colour_transfer);

    glBindImageTexture(8, texture_index, 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA32F);
    glBindTexture(GL_TEXTURE_1D, texture_index);
    // END -------------- creating 1d sampler for TRANSFER FUNCTION ------------- //
}

void GLWidget::floatGradientBind(GLuint texture_index)
{
    int XDIM=m_dataset.m_dimx, YDIM=m_dataset.m_dimy, ZDIM=m_dataset.m_dimz;

    glGenTextures(1, &texture_index);
    glActiveTexture(GL_TEXTURE9);
    glBindTexture(GL_TEXTURE_3D, texture_index);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    glTexImage3D(/* target          */ GL_TEXTURE_3D,
                 /* level           */ 0,
                 /* internal format */ GL_RGBA32F, // GL_R8UI,
                 /* dimensions      */ XDIM,YDIM,ZDIM,
                 /* border          */ 0,
                 /* format          */ GL_RGBA,
                 /* type            */ GL_FLOAT,
                 /* data            */ m_dataset.pGradient);
    glBindImageTexture(9, texture_index, 0, GL_TRUE, 0, GL_READ_ONLY, GL_RGBA32F);
    glGenerateMipmap( GL_TEXTURE_3D );
    glBindTexture(GL_TEXTURE_3D, texture_index);
}

void GLWidget::framebufferBind(GLuint texture_index)
{
    // -------------- creating 2d texture ------------- //
    // dimensions of the image

    glGenTextures(1, &texture_index);
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, texture_index);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, tex_w, tex_h, 0, GL_RGBA, GL_FLOAT, nullptr);
    glBindImageTexture(2, texture_index, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA32F);
}


void GLWidget::floatVolumeBind(GLuint texture_index)
{
    if(m_ess_enabled)
    {
        buildBinaryVolume(0.1f);
        buildSummedVolumeTable();

        this->kdTreeBuild();

        float* pDebugKDTree = new float[m_dataset.m_voxel_len];
        for (size_t i = 0; i < m_dataset.m_voxel_len; i++)
        {
           pDebugKDTree[i] = 0.0f;
        }
        m_current_node = 0;
        m_bounding_box_count = 1;
        this->countLeafNodes(&m_kdtree_root);
        float* bounding_boxes = new float[m_bounding_box_count*6];

        kdTreeVolumise(&m_kdtree_root, pDebugKDTree, bounding_boxes);

        for (size_t i = 0; i < m_dataset.m_voxel_len; i++)
        {
            if(isEdgeVoxel(i, m_dataset.m_dimx, m_dataset.m_dimy, m_dataset.m_dimz))
            {
                pDebugKDTree[i] = 0;
            }
        }

        // choose which dataset to colour:
        //    this->colourVoxels(pDebugKDTree);
//        this->colourVoxels(m_dataset.pfloatVolume);
        this->boundingboxDataBind(bounding_boxes, m_bounding_box_count);
    }
    else
    {// if empty-space-skipping is NOT enabled:
        // manually set bounding boxes ...
        // ... to enclose the entire data
        float full_bounding_box[6];
        full_bounding_box[0] = 0;
        full_bounding_box[1] = 0;
        full_bounding_box[2] = 0;
        full_bounding_box[3] = m_dataset.m_dimx-1;
        full_bounding_box[4] = m_dataset.m_dimy-1;
        full_bounding_box[5] = m_dataset.m_dimz-1;

        this->boundingboxDataBind(full_bounding_box, 1);
    }

    int XDIM=m_dataset.m_dimx, YDIM=m_dataset.m_dimy, ZDIM=m_dataset.m_dimz;
    //load data into a 3D texture
    glGenTextures(1, &texture_index);
    glActiveTexture(GL_TEXTURE10);

    glBindTexture(GL_TEXTURE_3D, texture_index);
    // set the texture sampling parameters
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    // change the interpolation mode from user choice:
    if(m_lerp)
    {
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    }
    else
    {
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    }

    glTexImage3D(/* target          */ GL_TEXTURE_3D,
                 /* level           */ 0,
                 /* internal format */ GL_R32F,
                 /* dimensions      */ XDIM,YDIM,ZDIM,
                 /* border          */ 0,
                 /* format          */ GL_RED,
                 /* type            */ GL_FLOAT,
                 /* data            */ m_dataset.pfloatVolume);

    glBindImageTexture(10, texture_index, 0, GL_TRUE, 0, GL_READ_ONLY, GL_R32F);
    glGenerateMipmap( GL_TEXTURE_3D );
    glBindTexture(GL_TEXTURE_3D, texture_index);
}

void GLWidget::floatRGBVolumeBind(GLuint texture_index)
{
    int XDIM=m_dataset.m_dimx, YDIM=m_dataset.m_dimy, ZDIM=m_dataset.m_dimz;
    //load data into a 3D texture
    glGenTextures(1, &texture_index);
    glActiveTexture(GL_TEXTURE11);

    glBindTexture(GL_TEXTURE_3D, texture_index);
    // set the texture sampling parameters
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
//    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    // cpu data interpolation:
    if(m_lerp)
    {
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    }
    else
    {
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    }

    glTexImage3D(/* target          */ GL_TEXTURE_3D,
                 /* level           */ 0,
                 /* internal format */ GL_RGBA32F,
                 /* dimensions      */ XDIM,YDIM,ZDIM,
                 /* border          */ 0,
                 /* format          */ GL_RGBA,
                 /* type            */ GL_FLOAT,
                 /* data            */ m_dataset.pfloatRGBVolume);
//                 /* data            */ pVolume1);
//                 /* data            */ pDebugKDTree);

//    glTexImage1D(GL_TEXTURE_1D, 0, GL_R32F, tf_data_len, 0, GL_RED, GL_FLOAT, colour_transfer);

    glBindImageTexture(11, texture_index, 0, GL_TRUE, 0, GL_READ_ONLY, GL_RGBA32F);
    glGenerateMipmap( GL_TEXTURE_3D );
    glBindTexture(GL_TEXTURE_3D, texture_index);
}

void GLWidget::buildBinaryVolume(float threshold)
{
    int layer = 0;
    pVolume1 = new float[m_dataset.m_voxel_len];
    for (size_t i = 0; i < m_dataset.m_voxel_len; i++)
    {
        float voxel_alpha = transfer_alpha(m_dataset.pfloatVolume[i], m_dataset.m_col_alphas);

        if(i%4096 == 0) layer++;
//        if (m_dataset.pfloatVolume[i] > 1.f)
        if (voxel_alpha > threshold)
        {
            pVolume1[i] = 1.f;
        }
        else
        {
            pVolume1[i] = 0.f;
        }
    }
}

void GLWidget::buildSummedVolumeTable()
{
    for(int i = 0; i < m_dataset.m_dimx; i++)
    {
        for(int j = 0; j < m_dataset.m_dimy; j++)
        {
            for(int k = 0; k < m_dataset.m_dimz; k++)
            {
                pVolume1[getIdx(i, j, k)] = sumVolumeTable(i, j, k);
            }
        }
    }
}

float GLWidget::sumVolumeTable(int i, int j, int k)
{
    if ((i == 0) || (j == 0) || (k == 0)) return 0.f;
    else
    {
        return pVolume1[getIdx(i,j,k)]   + pVolume1[getIdx(i-1,j-1,k-1)] + \
               pVolume1[getIdx(i-1,j,k)] - pVolume1[getIdx(i,j-1,k-1)] + \
               pVolume1[getIdx(i,j-1,k)] - pVolume1[getIdx(i-1,j,k-1)] + \
               pVolume1[getIdx(i,j,k-1)] - pVolume1[getIdx(i-1,j-1,k)];
    }
}

// get indices of 1D array, by passing 3D arrray coordinates i, j, k:
int GLWidget::getIdx(int i, int j, int k)
{
    return (k * m_dataset.m_dimx * m_dataset.m_dimy) + (j * m_dataset.m_dimx) + i;
}

// initialise K-d tree:
void GLWidget::initTree()
{
    m_kdtree_root.tn_depth = 0;
    int full_bounding_box[6] = {0, 0, 0,
                                m_dataset.m_dimx-1, m_dataset.m_dimy-1, m_dataset.m_dimz-1};
    int x_min = m_dataset.m_dimx-1, x_max = 0;
    int y_min = m_dataset.m_dimy-1, y_max = 0;
    int z_min = m_dataset.m_dimz-1, z_max = 0;

//    boundVolumeBox(full_bounding_box, x_min, x_max, y_min, y_max, z_min, z_max);
    boundVolumeBoxSummedVT(full_bounding_box, x_min, x_max, y_min, y_max, z_min, z_max);

    // bound the entire
    m_kdtree_root.u1 = x_min; //0;
    m_kdtree_root.v1 = y_min; //0;
    m_kdtree_root.w1 = z_min; //0;

    m_kdtree_root.u2 = x_max; //m_dataset.m_dimx-1;
    m_kdtree_root.v2 = y_max; //m_dataset.m_dimy-1;
    m_kdtree_root.w2 = z_max; //m_dataset.m_dimz-1;

    m_kdtree_root.tn_plane = 0;
    m_kdtree_root.tn_split_axis = 0;

    m_kdtree_root.tn_is_leaf = true;
    m_kdtree_root.tn_side = 'O';

    m_kdtree_root.tn_bounding_volume = computeVolumeBV(&m_kdtree_root);
    m_kdtree_root.tn_parent = nullptr;

    m_kdtree_root.tn_left_ptr  = nullptr;
    m_kdtree_root.tn_right_ptr = nullptr;
}

float GLWidget::getNumNonEmptyCells(int u1, int v1, int w1, int u2, int v2, int w2)
{
    return pVolume1[getIdx(u2, v2, w2)] - pVolume1[getIdx(u2, v2, w1)] - \
           pVolume1[getIdx(u1, v2, w2)] - pVolume1[getIdx(u1, v2, w1)] - \
           pVolume1[getIdx(u2, v1, w2)] - pVolume1[getIdx(u2, v1, w1)] + \
           pVolume1[getIdx(u1, v1, w2)] - pVolume1[getIdx(u1, v1, w1)];
}

void GLWidget::destroyTheTree(KDTree* node)
{
    if((node->tn_left_ptr == nullptr) && (node->tn_right_ptr == nullptr)
       && (node->tn_parent != nullptr))
    {
        free(node);
    }
    else if((node->tn_left_ptr != nullptr) && (node->tn_right_ptr != nullptr))
    {
        destroyTheTree(node->tn_left_ptr);
        destroyTheTree(node->tn_right_ptr);
        // node has no more children:
        node->tn_is_leaf = true;
    }
}

float GLWidget::computeBV(KDTree* node)
{
    return getNumNonEmptyCells(node->u1, node->v1, node->w1,
                               node->u2, node->v2, node->w2);
}

float GLWidget::splitCost(KDTree* node) // (1)
{
    // compute the cost of splitting
    // (computes and returns the sum of both children bounding boxes)
    return computeVolumeBV(node->tn_left_ptr) + computeVolumeBV(node->tn_right_ptr);
}

bool GLWidget::isDivisable(KDTree *node)
{
    bool flag = true;
    int dimu = abs(node->u2 - node->u1 + 1);
    int dimv = abs(node->v2 - node->v1 + 1);
    int dimw = abs(node->w2 - node->w1 + 1);

    // This needs to check two things:
    // 1. The node volume is smaller than some minimum threshold.
    //    (suggested as 10% of the volume, so nodes not smaller than 6x6x6 for 64x64x64)
    if (computeVolumeBV(node) < trunc(0.1 * double(m_dataset.m_dimx * m_dataset.m_dimy * m_dataset.m_dimz)))
    {
    return false;
    }
    if((dimu < 9) || (dimv < 9) || (dimw < 9)) return false;

    // 2. The best splitting plane can not remove enough ...
    //    ... empty space from the node (some threshold):
    float percentage_remove_empty_space;
    if(node->tn_parent != nullptr)
    {// if node is child of some parent:
        percentage_remove_empty_space = float(node->tn_parent->tn_bounding_volume - node->tn_bounding_volume)
                                        / float(node->tn_bounding_volume);
    }
    else
    { // if node is root
        percentage_remove_empty_space = float((m_dataset.m_dimx * m_dataset.m_dimy * m_dataset.m_dimz) - node->tn_bounding_volume)
                                         / float(node->tn_bounding_volume);
    }
    if(percentage_remove_empty_space < 0.1f)
    {
        return false;
    }

    if(node->tn_depth > 0)
    {
        flag = false;
    }
    return flag;
}

int GLWidget::selectSplittingPlane(KDTree *node, int dimx, int dimy, int dimz, float& min_cost, int& plane_axis)
{
    int start_plane[3] = {node->u1, node->v1, node->w1};
    int end_plane[3]   = {node->u2, node->v2, node->w2};

    plane_axis = getPlaneAxis(node);

    int plane_selection = 4;

    int x_min = dimx-1, x_max = 0;
    int y_min = dimy-1, y_max = 0;
    int z_min = dimz-1, z_max = 0;

    // choose best splitting plane from all available axis:
    for (int test_plane = start_plane[plane_axis]; test_plane < end_plane[plane_axis]; test_plane++)
    {
        KDTree* left_child_node  = generateLeftBV (node, test_plane, x_min, y_min, z_min,
                                                                     x_max, y_max, z_max);
        KDTree* right_child_node = generateRightBV(node, test_plane, x_min, y_min, z_min,
                                                                     x_max, y_max, z_max);

        float left_cost  = computeVolumeBV(left_child_node);
        float right_cost = computeVolumeBV(right_child_node);

        if (left_cost + right_cost < min_cost)
        {
            min_cost = left_cost + right_cost;
            plane_selection = test_plane;
        }
    }
    return plane_selection;
}

// how many voxels are bound by the Bounding Volume:
int GLWidget::computeVolumeBV(KDTree *node)
{
    int dimu = abs(node->u2 - node->u1 + 1);
    int dimv = abs(node->v2 - node->v1 + 1);
    int dimw = abs(node->w2 - node->w1 + 1);

    return dimu * dimv * dimw;
}

void GLWidget::triggerUpdate()
{
    m_update_colours_on_cpu = !m_update_colours_on_cpu;
}

void GLWidget::writeVolumeToFile(int size, GLubyte* volume)
{
    std::string readout = "data/readout10.raw";
    std::ofstream output;
    output.open (readout, std::ios::out | std::ios::binary);
    if (output.is_open())
    {
        std::cout << "file " << readout << " opened\n";
    }
    else std::cout << "file " << readout << " cannot be opened\n";

    for (int i = 0; i < size; i++)
    {
        output.write( reinterpret_cast<const char*>(&volume[i]), sizeof(char));
    }
}

float GLWidget::logScaleAlpha(float value)
{
    return float(exp(double(value)/22.0));
}

void GLWidget::splitNode(KDTree* node)
{
    // get index of plane to split on:
    if (isDivisable(node))
    {
        int plane_axis = 0;
        float cost = float(m_dataset.m_dimx) * float(m_dataset.m_dimy) * float(m_dataset.m_dimz);
        int plane = selectSplittingPlane(node, m_dataset.m_dimx, m_dataset.m_dimy, m_dataset.m_dimz, cost, plane_axis);

        node->tn_is_leaf = false;
        node->tn_plane = plane;
        node->tn_split_axis = plane_axis;

        // generate the two bounding volumes after the split:
        // (create two nodes and populate the boundary regions)
        generateBVs(node, plane);

        splitNode(node->tn_left_ptr);
        splitNode(node->tn_right_ptr);
    }
    else
    {
        node->tn_is_leaf = true;
        node->tn_plane = -1;
        node->tn_split_axis = -1;
    }
}

void GLWidget::kdTreeBuild()
{
    initTree();
    splitNode(&m_kdtree_root);
}

void GLWidget::kdTreePrintNode(KDTree* node)
{
    if(node->tn_is_leaf) return;
    else
    {
        kdTreePrintNode(node->tn_left_ptr);
        kdTreePrintNode(node->tn_right_ptr);
    }
}

void GLWidget::boundVolumeBoxMinMax(int bounding_box[6], int &x_min, int &x_max, int &y_min, int &y_max, int &z_min, int &z_max)
{
    for(int i = bounding_box[0]; i <= bounding_box[3]; i++)
    {
        for(int j = bounding_box[1]; j <= bounding_box[4]; j++)
        {
             for(int z = bounding_box[2]; z <= bounding_box[5]; z++)
             {
                if(pVolume2[getIdx(i, j, z)] > 0)
                {// if voxel is non-empty:
                    if (i < x_min) x_min = i;
                    if (i > x_max)
                    {
                        x_max = i;
                    }

                    if (j < y_min) y_min = j;
                    if (j > y_max) y_max = j;

                    if (z < z_min) z_min = z;
                    if (z > z_max) z_max = z;
                }
             }
        }
    }
}

void GLWidget::boundVolumeBoxSummedVT(int bounding_box[6], int &x_min, int &x_max, int &y_min, int &y_max, int &z_min, int &z_max)
{
    // x axis:
    // (obtaining u1)
    for(int i = bounding_box[0]; i <= bounding_box[3]; i++)
    {
        if(getNumR(i,i+1, bounding_box[1], bounding_box[4], bounding_box[2], bounding_box[5]) > 0)
        {
            x_min = i;
            break;
        }
    }
    // (obtaining u2)
    for(int i = bounding_box[3]; i >= bounding_box[0]; i--)
    {
        if(getNumR(i-1,i, bounding_box[1], bounding_box[4], bounding_box[2], bounding_box[5]) > 0)
        {
            x_max = i;
            break;
        }
    }
    // y axis:
    // (obtaining v1)
    for(int j = bounding_box[1]; j <= bounding_box[4]; j++)
    {
        if(getNumR(bounding_box[0], bounding_box[3], j,j+1, bounding_box[2], bounding_box[5]) > 0)
        {
            y_min = j;
            break;
        }
    }
    // (obtaining v2)
    for(int j = bounding_box[4]; j >= bounding_box[1]; j--)
    {
        if(getNumR(bounding_box[0], bounding_box[3], j-1,j, bounding_box[2], bounding_box[5]) > 0)
        {
            y_max = j;
            break;
        }
    }
    // z axis:
    // (obtaining w1)
    for(int z = bounding_box[2]; z <= bounding_box[5]; z++)
    {
        if(getNumR(bounding_box[0], bounding_box[3], bounding_box[1], bounding_box[4], z, z+1) > 0)
        {
            z_min = z;
            break;
        }
    }
    // (obtaining w2)
    for(int z = bounding_box[5]; z >= bounding_box[2]; z--)
    {
        float numR = getNumR(bounding_box[0], bounding_box[3], bounding_box[1], bounding_box[4], z-1, z);
        if(numR > 0)
        {
            z_max = z;
            break;
        }
    }
}

float GLWidget::getNumR(int u1, int u2, int v1, int v2, int w1, int w2)
{
    return (pVolume1[getIdx(u2, v2, w2)] - pVolume1[getIdx(u2, v2, w1)]) - \
           (pVolume1[getIdx(u1, v2, w2)] - pVolume1[getIdx(u1, v2, w1)]) - \
           (pVolume1[getIdx(u2, v1, w2)] - pVolume1[getIdx(u2, v1, w1)]) + \
           (pVolume1[getIdx(u1, v1, w2)] - pVolume1[getIdx(u1, v1, w1)]);
}

void GLWidget::colourVoxels()
{
    m_dataset.pfloatRGBVolume = new float[m_dataset.m_dimx*m_dataset.m_dimy*m_dataset.m_dimz*4];
    int idx = 0;
    for (int i = 0; i < m_dataset.m_dimx*m_dataset.m_dimy*m_dataset.m_dimz; i++)
    {
        RGBA col = transfer(m_dataset.pfloatVolume[i]/255.f*100, m_dataset.m_col_gradient, m_dataset.m_col_alphas);

        m_dataset.pfloatRGBVolume[idx]   = col.m_red;
        m_dataset.pfloatRGBVolume[idx+1] = col.m_green;
        m_dataset.pfloatRGBVolume[idx+2] = col.m_blue;
        m_dataset.pfloatRGBVolume[idx+3] = col.m_alpha;

        idx += 4;
    }
    m_colours_are_ready = true;
}

void GLWidget::colourVoxels(float* custom_dataset)
{
    m_dataset.pfloatRGBVolume = new float[m_dataset.m_dimx*m_dataset.m_dimy*m_dataset.m_dimz*4];
    int idx = 0;
    for (int i = 0; i < m_dataset.m_dimx*m_dataset.m_dimy*m_dataset.m_dimz; i++)
    {
        RGBA col = transfer(custom_dataset[i], m_dataset.m_col_gradient, m_dataset.m_col_alphas);

        m_dataset.pfloatRGBVolume[idx]   = col.m_red;
        m_dataset.pfloatRGBVolume[idx+1] = col.m_green;
        m_dataset.pfloatRGBVolume[idx+2] = col.m_blue;
        m_dataset.pfloatRGBVolume[idx+3] = col.m_alpha;

        idx += 4;
    }

    for(int i = 0; i < 40; i++)
    {
        qDebug() << "@m_dataset.pfloatRGBVolume[" << i << "] = " << m_dataset.pfloatRGBVolume[i];
    }
}

void GLWidget::kdTreePrint()
{
    qDebug() << "#Printing KD-Tree:";
    kdTreePrintNode(&m_kdtree_root);
}

int GLWidget::getPlaneAxis(KDTree* node)
{
    int dimu = abs(node->u2 - node->u1);
    int dimv = abs(node->v2 - node->v1);
    int dimw = abs(node->w2 - node->w1);

    int computed_max = std::max({dimu, dimv, dimw});

    if((dimu == dimv) && (dimu == dimw)) return 0;
    else if (dimu == computed_max)
    {
        return 0;
    }
    else if (dimv == computed_max)
    {
        return 1;
    }
    else if (dimw == computed_max)
    {
        return 2;
    }
    return -1;
}

void GLWidget::kdTreeVolumise(KDTree* node, float* volumisedKD, float* bounding_boxes)
{
    if(node->tn_is_leaf)
    {
        bounding_boxes[m_current_node] = float(node->u1);
        bounding_boxes[m_current_node+1] = float(node->v1);
        bounding_boxes[m_current_node+2] = float(node->w1);

        bounding_boxes[m_current_node+3] = float(node->u2);
        bounding_boxes[m_current_node+4] = float(node->v2);
        bounding_boxes[m_current_node+5] = float(node->w2);

        m_current_node += 6;
    }
    else
    {
        kdTreeVolumise(node->tn_left_ptr, volumisedKD, bounding_boxes);
        kdTreeVolumise(node->tn_right_ptr, volumisedKD, bounding_boxes);
    }
}

void GLWidget::countLeafNodes(KDTree* node)
{
    if(node->tn_is_leaf)
    {
        m_bounding_box_count++;
    }
    else
    {
        countLeafNodes(node->tn_left_ptr);
        countLeafNodes(node->tn_right_ptr);
    }
}


KDTree* GLWidget::generateLeftBV(KDTree *node, int plane_id,
                              int x_min, int y_min, int z_min,
                              int x_max, int y_max, int z_max)
{
    KDTree* left_child_node = new KDTree;
    int plane_axis = getPlaneAxis(node); //node->tn_depth % 3;

    int full_bounding_box[6] = {node->u1, node->v1, node->w1,
                                node->u2, node->v2, node->w2};

    full_bounding_box[plane_axis+3] = plane_id; // /= 2;


//    boundVolumeBox(full_bounding_box, x_min, x_max, y_min, y_max, z_min, z_max);
    boundVolumeBoxSummedVT(full_bounding_box, x_min, x_max, y_min, y_max, z_min, z_max);

    left_child_node->u1 = x_min;
    left_child_node->v1 = y_min;
    left_child_node->w1 = z_min;

    left_child_node->u2 = x_max;
    left_child_node->v2 = y_max;
    left_child_node->w2 = z_max;

    left_child_node->tn_depth = node->tn_depth + 1;

    left_child_node->tn_left_ptr  = nullptr;
    left_child_node->tn_right_ptr = nullptr;

    left_child_node->tn_plane = 0;
    left_child_node->tn_split_axis = 0;

    left_child_node->tn_is_leaf = true;
    left_child_node->tn_side = 'L';

    left_child_node->tn_bounding_volume = computeVolumeBV(left_child_node);
    left_child_node->tn_parent = node;

    return left_child_node;
}

KDTree* GLWidget::generateRightBV(KDTree *node, int plane_id,
                               int x_min, int y_min, int z_min,
                               int x_max, int y_max, int z_max)
{
    KDTree* right_child_node = new KDTree;

    int plane_axis = getPlaneAxis(node);
    int full_bounding_box[6] = {node->u1, node->v1, node->w1,
                                node->u2, node->v2, node->w2};

    full_bounding_box[plane_axis] = plane_id; // full_bounding_box[plane_axis+3] / 2;

//    boundVolumeBox(full_bounding_box, x_min, x_max, y_min, y_max, z_min, z_max);
    boundVolumeBoxSummedVT(full_bounding_box, x_min, x_max, y_min, y_max, z_min, z_max);

    right_child_node->u1 = x_min;
    right_child_node->v1 = y_min;
    right_child_node->w1 = z_min;

    right_child_node->u2 = x_max;
    right_child_node->v2 = y_max;
    right_child_node->w2 = z_max;

    right_child_node->tn_depth = node->tn_depth + 1;

    right_child_node->tn_left_ptr  = nullptr;
    right_child_node->tn_right_ptr = nullptr;

    right_child_node->tn_plane = 0;
    right_child_node->tn_split_axis = 0;
    right_child_node->tn_is_leaf = true;

    right_child_node->tn_side = 'R';

    right_child_node->tn_bounding_volume = computeVolumeBV(right_child_node);
    right_child_node->tn_parent = node;

    return right_child_node;

}

void GLWidget::generateBVs(KDTree *node, int plane)
{

    int x_min = m_dataset.m_dimx-1, x_max = 0;
    int y_min = m_dataset.m_dimy-1, y_max = 0;
    int z_min = m_dataset.m_dimz-1, z_max = 0;

    KDTree* left_child_node  = generateLeftBV(node, plane,  x_min, y_min, z_min,
                                                            x_max, y_max, z_max);
    KDTree* right_child_node = generateRightBV(node, plane, x_min, y_min, z_min,
                                                            x_max, y_max, z_max);
    // find

    // finally, set set-up pointers ...
    // ... to the populated left and right nodes:
    node->tn_left_ptr  = left_child_node;
    node->tn_right_ptr = right_child_node;
}

void GLWidget::slotGradient(Gradient newGradient)
{
    m_dataset.m_col_gradient = newGradient;
    emit(signalGradient(m_dataset.m_col_gradient));
}

void GLWidget::slotColourUpdatePacket(RGBA col_pack)
{
    // save previous gradient:
    m_undo_col_gradient = m_dataset.m_col_gradient;
    switch (col_pack.m_channel)
    {
    case RED:
        m_dataset.m_col_gradient.m_colour_gradient.find(col_pack.m_index)->second.m_red = col_pack.m_red;
        break;
    case GREEN:
        m_dataset.m_col_gradient.m_colour_gradient.find(col_pack.m_index)->second.m_green= col_pack.m_green;
        break;
    case BLUE:
        m_dataset.m_col_gradient.m_colour_gradient.find(col_pack.m_index)->second.m_blue= col_pack.m_blue;
        break;
    default:
        qDebug() << "WARNING: unknown colour channel received.";
        break;
    }

    emit(signalGradient( m_dataset.m_col_gradient ));

}

void GLWidget::mouseReleaseEvent(QMouseEvent* event)
{
    Ball_EndDrag(&m_arcball);
    m_mouse_pressed  = false;
}

void GLWidget::resizeGL(int w, int h)
{
    glViewport(0,0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0, double(w)/double(h), 0.01, 100.0);

    tex_w = 772; tex_h = 740;
    Ball_SetViewport(&m_arcball, w, h);

    // Set up a perspective view, with square aspect ratio
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
}

void GLWidget::displayVersionOpenGL()
{
    qDebug() << glGetString(GL_VERSION);
}

// functions to display device information, relevant to parallel computation:
void GLWidget::displayMaxWorkGroupCount()
{
    int work_grp_cnt[3];
    glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 0, &work_grp_cnt[0]);
    glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 1, &work_grp_cnt[1]);
    glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 2, &work_grp_cnt[2]);

    qDebug() << "@max global (total) work group counts x: "
             << work_grp_cnt[0] << " y: " << work_grp_cnt[1] << " z: " << work_grp_cnt[2];
}

void GLWidget::displayMaxWorkGroupSize()
{
    int work_grp_size[3];

    glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 0, &work_grp_size[0]);
    glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 1, &work_grp_size[1]);
    glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 2, &work_grp_size[2]);

    qDebug() << "@max local (in one shader) work group sizes x: "
             << work_grp_size[0] << " y: " << work_grp_size[1] << " z: " << work_grp_size[2];
}

void GLWidget::displayMaxWorkGroupInvocations()
{
    int work_grp_inv;
    glGetIntegerv(GL_MAX_COMPUTE_WORK_GROUP_INVOCATIONS, &work_grp_inv);
    qDebug() << "@max local work group invocations i: " << work_grp_inv;
}

int GLWidget::getCubeDistance(int dimension, int coordinate)
 {
     if (coordinate < dimension/2)
         return coordinate + 1;
     else
         return dimension - coordinate;
 }

// generate data for evaluation tests:
bool GLWidget::loadVolumeData()
{
    int XDIM=m_dataset.m_dimx, YDIM=m_dataset.m_dimy, ZDIM=m_dataset.m_dimz;

    const int size = XDIM*YDIM*ZDIM;
    m_dataset.pfloatVolume = new float[size];

    switch (m_chosen_dataset)
    {
    case 0:
        this->generateDiscreteNestedCubes();
    break;
    case 1:
        this->generateSmoothNestedCubes();
    break;
    case 2:
        this->generateVolumePlanes();
    break;
    }

    // compute optimal step size (assuming cube bounding box):
    float voxel_size = BOUNDING_CUBE_DIM/m_dataset.m_dimx;
    float optimal_step = voxel_size/2.f;

    m_step_size   = optimal_step;     // 1024
    if(m_step_size < SMALLEST_STEP_ALLOWED) m_step_size = SMALLEST_STEP_ALLOWED;
    m_max_steps   = float(ceil(sqrt(3) * double(BOUNDING_CUBE_DIM) / double(m_step_size)) );

    emit(signalOptimalStep( double(m_step_size) ));
    emit(signalOptimalMaxSteps( double(m_max_steps) ));

    return true;
}

void GLWidget::generateVolumePlanes()
{
    for(size_t x = 0; x < m_dataset.m_dimx; x++)
    {
//        qDebug() << "@making data - " << x << " / " << m_dataset.m_dimx;
        for(size_t y = 0; y < m_dataset.m_dimy; y++)
        {
            for(size_t z = 0; z < m_dataset.m_dimz; z++)
            {
                m_dataset.pfloatVolume[getIdx(int(x),int(y),int(z))] = 3.f+float(x)*4.f-1.f; //(float(x)*4-1)/256.f*100.f;
            }
        }
    }
}

void GLWidget::generateDiscreteNestedCubes()
{
    //    float colour_list[32] = {0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, // 5%
        float colour_list[32] = {12.75f, 12.75f, 12.75f, 12.75f, 12.75f, 12.75f, 12.75f, 12.75f, // 5%
                                 76.5f, 76.5f, 76.5f, 76.5f, 76.5f, 76.5f, 76.5f, 76.5f, // 30%
                                 127.5, 127.5, 127.5, 127.5, 127.5, 127.5, 127.5, 127.5, // 50%
                                 204, 204, 204, 204, 204, 204, 204, 204,}; // 80%

        for(int distance = 1; distance < m_dataset.m_dimx/2; distance++)
        {
//            qDebug() << "@making data - " << distance << " / " << m_dataset.m_dimx/2;
            for(int x = m_dataset.m_dimx-distance; x > distance ; x--)
            {
                for(int y = m_dataset.m_dimy-distance; y > distance ; y--)
                {
                    for(int z = m_dataset.m_dimz-distance; z > distance ; z--)
                    {
                        m_dataset.pfloatVolume[getIdx(x,y,z)] = colour_list[distance];
                    }
                }
            }
        }
}

void GLWidget::generateSmoothNestedCubes()
{
    int mindistance = 0, dist = 0;
    float divisor = 256.f / (m_dataset.m_dimx/2);
    for(int x = 0; x < int(m_dataset.m_dimx); x++)
    {
//        qDebug() << "@making data - " << x << " / " << int(m_dataset.m_dimx);
        for(int y = 0; y < int(m_dataset.m_dimy); y++)
        {
            for(int z = 0; z < int(m_dataset.m_dimz); z++)
            {
                mindistance = getCubeDistance(int(m_dataset.m_dimx), x);
                dist = getCubeDistance(int(m_dataset.m_dimy), y);

                if (mindistance > dist)
                    mindistance = dist;

                dist = getCubeDistance(int(m_dataset.m_dimz), z);

                if (mindistance > dist)
                    mindistance = dist;

                m_dataset.pfloatVolume[getIdx(x,y,z)] = float(mindistance*divisor);
//                m_dataset.pfloatVolume[getIdx(x,y,z)] = 0.f;
            }

        }
    }
}

int GLWidget::parseDataSizeFromFilename(QString filename)
{
    QRegularExpression re;
    re.setPattern("_(\\d*)x(\\d*)x(\\d*)_");
    QRegularExpressionMatch match = re.match(filename);

    int dimx, dimy, dimz;

    if (match.hasMatch())
    {
        dimx = match.captured(1).toInt();
        dimy = match.captured(2).toInt();
        dimz = match.captured(3).toInt();
    }
    else
    {
        qDebug() << "ERROR: name file must contain data dimensions in format:";
        qDebug() << "_'xdim'x'ydim'x'zdim'_";
        qDebug() << "for example, _64x64x64_";
        dimx = dimy = dimz = 0;
    }

    int largest = 0;
    if ((dimx >= dimy) && (dimx >= dimz))
        largest = dimx;
    else if ((dimy >= dimx) && (dimy >= dimz))
        largest = dimy;
    else largest = dimz;

    return largest;
}

bool GLWidget::loadVolumeData(QString filename)
{
    qDebug() << "[loadVolumeData] loading file: " << filename
             << m_dataset.m_dimx << " " << m_dataset.m_dimy << " " << m_dataset.m_dimz
             << m_dataset.m_voxel_len;

    int size = m_dataset.m_dimx*m_dataset.m_dimy*m_dataset.m_dimz;

    FILE* pFile = fopen(filename.toStdString().c_str(),"rb");
    if(pFile == nullptr)
    {
        qDebug() << "could not open file " << filename;
        return false;
    }

    GLubyte* pVolume = new GLubyte[size];
    m_dataset.pfloatVolume = new float[size];
    // don't read from file for testing:
    fread(pVolume,sizeof(GLubyte),size_t(size),pFile);
    fclose(pFile);

    for (int i = 0; i < size; i++)
    {// convert data to float:
        m_dataset.pfloatVolume[i] = float(pVolume[i]);
    }

    // since new data was loaded, colours were not precomputed yet:
    m_colours_are_ready = false;

    // compute optimal step size (assuming cube bounding box):
    float voxel_size = BOUNDING_CUBE_DIM/m_dataset.m_dimx;
    float optimal_step = voxel_size/2.f;

    m_step_size   = optimal_step;
    if(m_step_size < SMALLEST_STEP_ALLOWED) m_step_size = SMALLEST_STEP_ALLOWED;
    m_max_steps   = float(ceil(sqrt(3) * double(BOUNDING_CUBE_DIM) / double(m_step_size)) );

    emit(signalOptimalStep( double(m_step_size) ));
    emit(signalOptimalMaxSteps( double(m_max_steps) ));

    delete[] pVolume;
    return true;
}

void GLWidget::precomputeGradient()
{
    // storing x, y, z as 3 RGB channels
    m_dataset.pGradient = new float[m_dataset.m_voxel_len*4];

    int layer_size = m_dataset.m_dimx * m_dataset.m_dimy;

    int voxel = 0;
    for (size_t i = 0; i < m_dataset.m_voxel_len*4; i+=4)
    {
        if(isEdgeVoxel(size_t(voxel), m_dataset.m_dimx, m_dataset.m_dimy, m_dataset.m_dimz))
        {
            // set to 0
            m_dataset.pGradient[i] = 255.f;
            m_dataset.pGradient[i+1] = 255.f;
            m_dataset.pGradient[i+2] = 255.f;
            m_dataset.pGradient[i+3] = 0.f;
        }
        else
        {
            // compute x gradient
            m_dataset.pGradient[i+1]   = -0.5f * (m_dataset.pfloatVolume[voxel+1] - m_dataset.pfloatVolume[voxel-1]);//  255.f;
            m_dataset.pGradient[i] = -0.5f * (m_dataset.pfloatVolume[voxel+m_dataset.m_dimy] - m_dataset.pfloatVolume[voxel-m_dataset.m_dimy]); // / 255.f;
            m_dataset.pGradient[i+2] = -0.5f * (m_dataset.pfloatVolume[voxel+layer_size] - m_dataset.pfloatVolume[voxel-layer_size]); // / 255.0;
            m_dataset.pGradient[i+3] = float(sqrt(double(m_dataset.pGradient[i]*m_dataset.pGradient[i] + m_dataset.pGradient[i+1]*m_dataset.pGradient[i+1] + m_dataset.pGradient[i+2] * m_dataset.pGradient[i+2])));
        }
        voxel++;
    }
}

bool GLWidget::isEdgeVoxel(size_t index, unsigned short dimx, unsigned short dimy, unsigned short dimz)
{
    size_t layer_size = dimx * dimy;
    size_t index_within_layer = index % layer_size;

    // check conditions in turn and return immediately if any one of them is met:
    if(index_within_layer <= dimx) return true;              // first row
    if(int(index_within_layer) - int(dimx*(dimy-1)) >= 0) return true; // last row
    if(index % dimx == 0) return true;                       // first col
    if(index % dimx == size_t(dimx-1)) return true;          // last col
    if(index < layer_size) return true;                      // bottom layer
    if(index > size_t(dimx*dimy*(dimz-1))) return true;      // top layer

    return false;
}

void GLWidget::slotAddNewPoint(RGBA newPoint)
{
    m_dataset.m_col_gradient.addPoint(newPoint.m_index, newPoint.m_red, newPoint.m_green, newPoint.m_blue);
    emit(signalGradient( m_dataset.m_col_gradient ));
}

void GLWidget::slotRemovePoint(float oldPoint)
{
    m_dataset.m_col_gradient.removePoint(oldPoint);

    emit(signalGradient( m_dataset.m_col_gradient ));
}

void GLWidget::slotAddNewPoint2(RGBA newPoint)
{
    m_dataset.m_col_gradient.addPoint(newPoint.m_index, newPoint.m_red, newPoint.m_green, newPoint.m_blue);

    emit(signalGradient2( m_dataset.m_col_gradient ));
}

void GLWidget::slotRemovePoint2(float oldPoint)
{
    m_dataset.m_col_gradient.removePoint(oldPoint);

    emit(signalGradient2( m_dataset.m_col_gradient ));
}

void GLWidget::slotAddAlphaPoint(Alpha newPoint)
{
    m_dataset.m_col_alphas.addAlphaPoint(newPoint.m_x, newPoint.m_y);
    emit(signalAlpha(m_dataset.m_col_alphas));
    if(m_BBupdate_enabled)
    {
        this->floatVolumeBind(m_samplevolumeID);
    }
}

void GLWidget::slotRemoveAlphaPoint(float oldPoint)
{
    m_dataset.m_col_alphas.removeAlphaPoint(oldPoint);
    emit(signalAlpha(m_dataset.m_col_alphas));
}

void GLWidget::slotAddAlphaPoint2(Alpha newPoint)
{
    m_dataset.m_col_alphas.m_x = newPoint.m_x;
    m_dataset.m_col_alphas.m_y = newPoint.m_y;
    m_dataset.m_col_alphas.addAlphaPoint(newPoint.m_x, newPoint.m_y);
    emit(signalAlpha2(m_dataset.m_col_alphas));
}

void GLWidget::slotRemoveAlphaPoint2(float oldPoint)
{
    m_dataset.m_col_alphas.m_x = oldPoint;
    m_dataset.m_col_alphas.removeAlphaPoint(oldPoint);
    emit(signalAlpha2(m_dataset.m_col_alphas));
}

void GLWidget::updateAngle()
{
  this->m_frame++;
  this->repaint();
}

void GLWidget::updateDatasetFile(QString data_file)
{
//    m_dataset.m_filename = data_file.toStdString().c_str();
    qDebug() << "got filename: " << data_file;

    unsigned short cube_dimension = static_cast<unsigned short>(parseDataSizeFromFilename(data_file));
    int size = cube_dimension * cube_dimension * cube_dimension;
    // bounding all data by a cube:
//    m_dataset.m_dimx = m_dataset.m_dimy = m_dataset.m_dimz = static_cast<unsigned short>(cube_dimension);
//    m_dataset.m_voxel_len = size_t(size);

    m_dataset.changeAllData2(data_file, size_t(size),
                            cube_dimension, cube_dimension, cube_dimension);

    qDebug() << m_dataset.m_filename << ", "
             << m_dataset.m_dimx << " " << m_dataset.m_dimy << " " << m_dataset.m_dimz << " "
             << m_dataset.m_voxel_len;

    // load data to texture:
    this->loadVolumeData(m_dataset.m_filename);

    this->precomputeGradient();

    glBindTexture(GL_TEXTURE_1D, 0);
    glDeleteTextures(1, &m_camera_data_tex);

    glBindTexture(GL_TEXTURE_3D, 0);
    glDeleteTextures(1, &m_samplevolumeID);

    glBindTexture(GL_TEXTURE_3D, 0);
    glDeleteTextures(1, &m_gradient_data_sampler);


    //    unsigned int samplevolumeID = 10;
    this->floatVolumeBind(m_samplevolumeID);

    if(m_update_colours_on_cpu)
    {
        this->colourVoxels();
        this->floatRGBVolumeBind(m_sampleRGBAvolumeID);
        // reset the flag
        m_update_colours_on_cpu = false;
    }

    //    unsigned int gradient_data_sampler = 9;
    this->floatGradientBind(m_gradient_data_sampler);

}

void GLWidget::updateTestDataset()
{
    unsigned short cube_dimension = 64;
    int size = cube_dimension * cube_dimension * cube_dimension;

    m_dataset.changeAllData2("Demo_64x64x64_file", size_t(size),
                            cube_dimension, cube_dimension, cube_dimension);

    qDebug() << m_dataset.m_filename << ", "
             << m_dataset.m_dimx << " " << m_dataset.m_dimy << " " << m_dataset.m_dimz << " "
             << m_dataset.m_voxel_len;

    // load data to texture:
    this->loadVolumeData();
    this->precomputeGradient();

    glBindTexture(GL_TEXTURE_1D, 0);
    glDeleteTextures(1, &m_camera_data_tex);

    glBindTexture(GL_TEXTURE_3D, 0);
    glDeleteTextures(1, &m_samplevolumeID);

    glBindTexture(GL_TEXTURE_3D, 0);
    glDeleteTextures(1, &m_gradient_data_sampler);


    //    unsigned int samplevolumeID = 10;
    this->floatVolumeBind(m_samplevolumeID);

    if(m_update_colours_on_cpu)
    {
        this->colourVoxels();
        this->floatRGBVolumeBind(m_sampleRGBAvolumeID);
        // reset the flag
        m_update_colours_on_cpu = false;
    }

    //    unsigned int gradient_data_sampler = 9;
    this->floatGradientBind(m_gradient_data_sampler);

}

void GLWidget::slotResetColours()
{
//    m_dataset.setGradient(Gradient(filename));
    m_dataset.setGradient(Gradient("table-colours.txt"));
    emit(signalGradient( m_dataset.m_col_gradient ));
}

void GLWidget::slotResetTF()
{
    m_dataset.setAlpha(Alpha("table-opacity.txt"));
//    m_dataset.setAlpha(Alpha(filename));
    emit(signalAlpha(m_dataset.m_col_alphas));
}

void GLWidget::slotUndoAlphas(Alpha oldAlpha)
{
    m_dataset.setAlpha(oldAlpha);
    emit(signalAlpha(m_dataset.m_col_alphas));
}

void GLWidget::slotUndoColour(Gradient oldGradient)
{
    m_dataset.setGradient(oldGradient);
    emit(signalGradient(m_dataset.m_col_gradient));
}

int getDigitN(int number, int digit)
{
    return number / int(pow(10,digit-1)) % 10;
}

void GLWidget::slotLightingInfo(int light_info)
{
    m_ambient_on  = getDigitN(light_info, 1);
    m_diffuse_on  = getDigitN(light_info, 2);
    m_specular_on = getDigitN(light_info, 3);
}

void GLWidget::slotClampStart(float clamp_start)
{
    m_clamp_start = clamp_start;
}

void GLWidget::slotClampEnd(float clamp_end)
{
    m_clamp_end = clamp_end;
}

void GLWidget::slotStepSize(float step_size)
{
    m_step_size = step_size;
}

void GLWidget::slotMaxSteps(float max_steps)
{
    m_max_steps = max_steps;
}


void GLWidget::slotColourFile(QString colour_file)
{
    m_dataset.setGradient(Gradient(colour_file.toStdString().c_str()));
    emit(signalGradient( m_dataset.m_col_gradient ));
}

void GLWidget::slotAlphaFile(QString alpha_file)
{
    m_dataset.setAlpha(Alpha(alpha_file.toStdString().c_str()));
    emit(signalAlpha(m_dataset.m_col_alphas));
}

void GLWidget::slotDataFile(QString data_file)
{
    qDebug() << "received data file: " << data_file;
    QString filename = "data/"+data_file.section('/', -1);
    qDebug() << "now updated: data file: " << filename;
    this->updateDatasetFile(filename);
}


