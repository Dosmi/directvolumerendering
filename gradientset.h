#pragma once
#ifndef GRADIENTSET_H
#define GRADIENTSET_H

#include <QWidget>
#include <QMouseEvent>
#include "gradient.h"
#include "alpha.h"
#include "rgba.h"
#include <vector>

struct ControlPoint1
{
    unsigned int x;
    unsigned int y;
    float value;
    RGBA col;
};
//#include "ControlPoint1.h"

class GradientSet : public QWidget
{
    Q_OBJECT
public:
    explicit GradientSet(QWidget *parent = nullptr);
    void paintEvent(QPaintEvent*) override;
    void mousePressEvent(QMouseEvent *event) override;
//    void mouseMoveEvent(QMouseEvent *event) override;
//    void mouseReleaseEvent(QMouseEvent *event) override;


    RGBA transfer(float value, Gradient col_gradient, Alpha opacities);
    void drawSelectorRGB(QPainter* painter, int x, int y, int z, int j);

    void sendLightInfo(int light_info);
    void sendClampNoiseStart(float clamp_start);
    void sendClampNoiseEnd(float clamp_end);
    void sendStepSize(float step_size);
    void sendMaxSteps(float max_steps);
    void sendColourFile(QString colour_file);
    void sendAlphaFile(QString alpha_file);
    void sendDataFile(QString data_file);

    Gradient m_col_gradient;
    bool approxCompare(float a, float b, float tolerancy);
    void undoColour();
    Gradient m_undo_col_gradient;
    Alpha    m_col_alphas;

    int m_stretch;
    float m_active_value;

    RGBA m_col_packet;

    bool m_clicked;
    bool m_dragging;

    float m_dragged_x;
    float m_dragged_y;

    float m_previous_x;
    float m_original_x;
    RGBA m_original_col;

    float m_prev_nbor;
    float m_next_nbor;

//    QPainter painter;

    std::vector<ControlPoint1> m_ctrl_pts;
    std::vector<ControlPoint1> m_undo_ctrl_pts;


signals:
    void signalActiveValue(float newActiveValue);
    void signalSetDialog(float newDialogValue);
    void signalSetRedSpinbox(RGBA newRedValue);
    void signalSetGreenSpinbox(RGBA newGreenValue);
    void signalSetBlueSpinbox(RGBA newBlueValue);

    void signalAddNewPoint(RGBA newPoint);
    void signalRemovePoint(float oldPoint);
    void signalAddNewPoint2(RGBA newPoint);
    void signalRemovePoint2(float oldPoint);

    void signalLockInput();
    void signalUnlockInput();

    void signalUndoColour(Gradient oldGradient);
    void signalLightingInfo(int light_info);

    void signalClampStart(float clamp_start);
    void signalClampEnd(float clamp_end);
    void signalStepSize(float step_size);
    void signalMaxSteps(float max_steps);
    void signalAlphaFile(QString alpha_file);
    void signalColourFile(QString colour_file);
    void signalDataFile(QString colour_file);

public slots:
    void slotGradientRed(float newGradient);
    void slotGradient(Gradient newGradient);
    void slotAlpha(Alpha newAlpha);
};

#endif // GRADIENTSET_H
