#include "alpha.h"
#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>

Alpha::Alpha()
{}

Alpha::Alpha(const char* filename)
{
    std::ifstream infile(filename);

    float point, alpha_value;

    while (infile >> point >> alpha_value)
    {
        this->addAlphaPoint(point, alpha_value);
    }
}

void Alpha::addAlphaPoint(float point, float alpha_value)
{
    m_alpha.insert(std::pair<float, float>(point, alpha_value));
}

void Alpha::removeAlphaPoint(float point)
{
    auto it=m_alpha.find(point);
    if(it != m_alpha.end()) m_alpha.erase(it);
}

void Alpha::debug()
{
    std::cout << "Alpha-point-alpha-pairs:\n";
    for (auto it : m_alpha)
    {
        std::cout << it.first << " -> " << it.second << "\n";
    }
}

