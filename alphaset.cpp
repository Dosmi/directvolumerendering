#include "alphaset.h"
#include <iostream>

RGBA AlphaSet::transferalpha(float value, Gradient col_gradient, Alpha opacities)
{
    float lerp_red, lerp_green, lerp_blue, lerp_alpha;

    // there are two disjoint arrays:
    // ... col_gradient - containing the control points for the RGB colours, ...
    // ... opacities    - containing the control points for the opacities

    // here, for a given value, we find two neighbours to linearly interpolate between ...
    // ... (for colour and opacity values):
    float lower_col_value = std::prev((col_gradient.m_colour_gradient.lower_bound(value)),1)->first;
    float upper_col_value = col_gradient.m_colour_gradient.lower_bound(value)->first;

    float lower_alpha_value = std::prev((opacities.m_alpha.lower_bound(value)),1)->first;
    float upper_alpha_value = opacities.m_alpha.lower_bound(value)->first;

//    std::cout << "neighbours of " << value << ": " << lower_col_value << ", " << upper_col_value << " -> ";

    RGBA lower_nbor_colour    = std::prev((col_gradient.m_colour_gradient.lower_bound(value)),1)->second;
    RGBA upper_nbor_colour    = col_gradient.m_colour_gradient.lower_bound(value)->second;
    float lower_nbor_alpha = std::prev((opacities.m_alpha.lower_bound(value)),1)->second;
    float upper_nbor_alpha = opacities.m_alpha.lower_bound(value)->second;

    float col_weight   = (value - lower_col_value)/(upper_col_value - lower_col_value);
    float alpha_weight = (value - lower_alpha_value)/(upper_alpha_value - lower_alpha_value);

//    std::cout << " weight: " << col_weight << ", starting: " << lower_nbor_colour.getRGBAString() << ",      ";

    lerp_red   = (1-col_weight) * lower_nbor_colour.m_red   + col_weight * upper_nbor_colour.m_red;
    lerp_green = (1-col_weight) * lower_nbor_colour.m_green + col_weight * upper_nbor_colour.m_green;
    lerp_blue  = (1-col_weight) * lower_nbor_colour.m_blue  + col_weight * upper_nbor_colour.m_blue;

    lerp_alpha = (1-alpha_weight) * lower_nbor_alpha + alpha_weight * upper_nbor_alpha;

//    std::cout << lerp_red << " " << lerp_green << " " << lerp_blue << "\n";

    // return the full colour, given in RGBA ...
    // ... (with given 'alpha' being in %, hence division by 100 and multiplication by 255)
    return RGBA(lerp_red, lerp_green, lerp_blue, (lerp_alpha/100)*255);
}


AlphaSet::AlphaSet(QWidget *parent)
    : QWidget(parent), m_stretch(2.8f),
      m_dragging(false), m_dragged_x(0.f), m_dragged_y(0.f)
{}

void AlphaSet::drawSelectorRGB(QPainter *painter, int x, int y, int z, int j)
{// draw the control point selector (circle button)
  painter->setPen(Qt::black);

  painter->setBrush(Qt::white);
  painter->drawEllipse(x-3, y-3, z+6, j+6);

  painter->setBrush(Qt::black);
  painter->drawEllipse(x, y, z, j);
}

void AlphaSet::drawLine(QPainter* painter, int x, int y, int z, int j)
{
    painter->drawLine(x, y, z, j);
}

void AlphaSet::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    float previous_x = 0.f;
    float previous_y = 0.f;

    RGBA rect_slice_col(255.f, 255.f, 255.f);
    // saved the stretch factor locally as int ...
    // ... since QT QRect takes in integer values
    int local_stretch = int(m_stretch);
    int red, green, blue, alpha;
    for (int i_column = 0 ; i_column < 100*m_stretch; i_column+=local_stretch)
    {
        QRect rect(i_column,1, local_stretch,100);
        // get the colour components (linear interpolation) of a rectangle slice:
        rect_slice_col = transferalpha(i_column/m_stretch, m_col_gradient, m_col_alphas);

        red = int(rect_slice_col.m_red  );
        green = int(rect_slice_col.m_green);
        blue = int(rect_slice_col.m_blue );
        alpha = int(rect_slice_col.m_alpha);

        QColor c = QColor(red, green, blue, alpha);

        // fill with color for the pixel
        painter.fillRect(rect, QBrush(c));
    }
    // draw bottom line (at opacity value 0, ...
    // ... indicating the user cannot drag below that)
    this->drawLine(&painter,
                   0, 100,
                   int(100.f*m_stretch), 100);

    // draw the selectors and lines between them:
    int i = 0;
    for (auto alpha : m_col_alphas.m_alpha)
    {
        m_ctrl_pts.push_back({alpha.first*m_stretch, 100.f-alpha.second, alpha.first, alpha.second});
        drawSelectorRGB(&painter,
                        int(alpha.first*m_stretch), int(100.f-alpha.second),
                        5,5);

        if (i)
        {
            this->drawLine(&painter,
                           int(previous_x), int(100.f-previous_y),
                           int(alpha.first*m_stretch), int(100.f-alpha.second));
        }

        previous_x = alpha.first*m_stretch;
        previous_y = alpha.second;
        i++;
    }

}

// slots for receiving data sent from other widgets:
void AlphaSet::slotGradient(Gradient newGradient)
{
    m_col_gradient = newGradient;
    repaint();
}

void AlphaSet::slotAlpha(Alpha newAlpha)
{
    // save the last one for undo-ing:
//    m_undo_col_alphas = m_col_alphas;
    m_col_alphas = newAlpha;
    repaint();
}

void AlphaSet::undoAlpha()
{
    m_col_alphas = m_undo_col_alphas;
    emit(signalUndoAlpha(m_undo_col_alphas ));
    repaint();
}

void AlphaSet::mouseDoubleClickEvent(QMouseEvent* event)
{// double (left) click event adds an opacity control point:
    int x = event->x(), y = event->y();

    if ( event->button() == Qt::LeftButton )
    {
        if((y > 0) && (y<100))
        {
            float add_y = 100.f - y;
            float add_x = x/m_stretch;

            // update locally, and send new information:
            m_col_alphas.addAlphaPoint(add_x, add_y);
            emit(signalAddAlphaPoint2(m_col_alphas));
        }
    }
    repaint();
}

void AlphaSet::mousePressEvent(QMouseEvent *event)
{
    int x = event->x(), y = event->y();
    if ( (y < 100) && (!m_dragging) )
    {
        for (ControlPoint cp : m_ctrl_pts)
        {
            // check if mouse clicked NEAR (5px) the control point:
            if ( (abs(int(cp.x - x)) < 5) && (abs(int(cp.y - y)) < 5) )
            {
                m_dragging = true;
                m_dragged_y = 100.f - y;
                m_dragged_x = cp.ct_number;

                m_original_x = cp.ct_number;
                m_previous_x = m_original_x;

                // save current value as previous value for 'undo':
                m_undo_col_alphas = m_col_alphas;

                m_prev_nbor = std::prev((m_col_alphas.m_alpha.lower_bound(m_original_x)),1)->first;
                m_next_nbor = m_col_alphas.m_alpha.upper_bound(m_original_x)->first;
            }
        }

    }
}

void AlphaSet::mouseMoveEvent(QMouseEvent *event)
{/* when user moves the mouse that is pressed down, ...
    ... start adding points where the mouse is moving ...
    ... and remove points from last mouse movement */
    // get current mouse coordinates from the event:
    int x = event->x(), y = event->y();

    if( (m_dragging) // if the mouse is pressed down, and not out of range ...
        && (x > m_prev_nbor * m_stretch) && (x < m_next_nbor * m_stretch)
        && (y > 0) && (y<100))
    {
        // remember this as the previous marker, ...
        // ... which will marked for removal
        m_previous_x = m_dragged_x;

        // set dragged coordinate and alpha pairs ...
        // value x maps to opacity y:
        m_dragged_y = 100.f - y;   // not taking the pure mouse y coordinate
        m_dragged_x = x/m_stretch; // stretch, since this widget is more than 100px

        // add colours locally:
        m_col_alphas.addAlphaPoint(m_dragged_x, m_dragged_y);
        m_col_alphas.removeAlphaPoint(m_previous_x);

        // send out this updated information:
        emit(signalRemoveAlphaPoint2(m_original_x));
        emit(signalAddAlphaPoint2(m_col_alphas));
    }
    repaint();
}

void AlphaSet::mouseReleaseEvent(QMouseEvent *event)
{// set alpha value when user releases the mouse
    if(m_dragging)
    {   // no longer dragging
        m_dragging = false;

        // set alpha where the user dragged the mouse:
        m_col_alphas.m_x = m_dragged_x;
        m_col_alphas.m_y = m_dragged_y;
        emit(signalAddAlphaPoint(m_col_alphas));

        // signal to remove previous point
        // (from previous mouse move event):
        emit(signalRemoveAlphaPoint(m_original_x));
    }
    repaint();
}
