#ifndef TRANSFERFUNCTION_H
#define TRANSFERFUNCTION_H

#include "rgba.h"
#include <cstdint>
#include <map>
#include "gradient.h"
#include "alpha.h"
#include <QColor>

#include <iterator>
#include <iostream>


// the transfer function - takes in a value, gets the colour and alpha value:
RGBA transfer(float value, Gradient col_gradient)
{
    float red, green, blue;

    if(col_gradient.m_colour_gradient.find(value) != col_gradient.m_colour_gradient.end())
    {
        std::cout << "exact: " << value << "\n";
        red   = col_gradient.m_colour_gradient.find(value)->second.m_red;
        green = col_gradient.m_colour_gradient.find(value)->second.m_green;
        blue  = col_gradient.m_colour_gradient.find(value)->second.m_blue;
    }

    else
    {
        float lower_value = std::prev((col_gradient.m_colour_gradient.lower_bound(value)),1)->first;
        float upper_value = col_gradient.m_colour_gradient.lower_bound(value)->first;

        std::cout << "neighbours of " << value << ": " << lower_value << ", " << upper_value << " -> ";

        RGBA lower = std::prev((col_gradient.m_colour_gradient.lower_bound(value)),1)->second;
        RGBA upper =col_gradient.m_colour_gradient.lower_bound(value)->second;

        float weight = (value - lower_value)/(upper_value - lower_value);

        std::cout << " weight: " << weight << ", starting: " << lower.getRGBAString() << ",      ";

        red   = (1-weight) * lower.m_red   + weight * upper.m_red;
        green = (1-weight) * lower.m_green + weight * upper.m_green;
        blue  = (1-weight) * lower.m_blue  + weight * upper.m_blue;

        std::cout << red << " " << green << " " << blue << "\n";
    }
    return RGBA(red, green, blue, 255.0);
}


// the transfer function - takes in a value, gets the colour and alpha value:
RGBA transfer(float value, Gradient col_gradient, Alpha opacities)
{
    float lerp_red, lerp_green, lerp_blue, lerp_alpha;

    // there are two disjoint arrays:
    // ... col_gradient - containing the control points for the RGB colours, ...
    // ... opacities    - containing the control points for the opacities

    // here, for a given value, we find two neighbours to linearly interpolate between ...
    // ... (for colour and opacity values):
    float lower_col_value = std::prev((col_gradient.m_colour_gradient.lower_bound(value)),1)->first;
    float upper_col_value = col_gradient.m_colour_gradient.lower_bound(value)->first;

    float lower_alpha_value = std::prev((opacities.m_alpha.lower_bound(value)),1)->first;
    float upper_alpha_value = opacities.m_alpha.lower_bound(value)->first;

//    std::cout << "neighbours of " << value << ": " << lower_col_value << ", " << upper_col_value << " -> ";

    RGBA lower_nbor_colour    = std::prev((col_gradient.m_colour_gradient.lower_bound(value)),1)->second;
    RGBA upper_nbor_colour    = col_gradient.m_colour_gradient.lower_bound(value)->second;
    float lower_nbor_alpha = std::prev((opacities.m_alpha.lower_bound(value)),1)->second;
    float upper_nbor_alpha = opacities.m_alpha.lower_bound(value)->second;

    float col_weight   = (value - lower_col_value)/(upper_col_value - lower_col_value);
    float alpha_weight = (value - lower_alpha_value)/(upper_alpha_value - lower_alpha_value);

//    std::cout << " weight: " << col_weight << ", starting: " << lower_nbor_colour.getRGBAString() << ",      ";

    lerp_red   = (1-col_weight) * lower_nbor_colour.m_red   + col_weight * upper_nbor_colour.m_red;
    lerp_green = (1-col_weight) * lower_nbor_colour.m_green + col_weight * upper_nbor_colour.m_green;
    lerp_blue  = (1-col_weight) * lower_nbor_colour.m_blue  + col_weight * upper_nbor_colour.m_blue;

    lerp_alpha = (1-alpha_weight) * lower_nbor_alpha + alpha_weight * upper_nbor_alpha;

//    std::cout << lerp_red << " " << lerp_green << " " << lerp_blue << "\n";

    // return the full colour, given in RGBA ...
    // ... (with given 'alpha' being in %, hence division by 100 and multiplication by 255)
    return RGBA(lerp_red, lerp_green, lerp_blue, (lerp_alpha/100)*255);
}

// the transfer function - takes in a value, gets the colour and alpha value:
float transfer(float value, Alpha opacities)
{
    float alpha;

    if(opacities.m_alpha.find(value) != opacities.m_alpha.end())
    {
        alpha = opacities.m_alpha.find(value)->second;
    }

    else
    {
        float lower_alpha_value = std::prev((opacities.m_alpha.lower_bound(value)),1)->first;
        float upper_alpha_value = opacities.m_alpha.lower_bound(value)->first;

        float lower_alpha = std::prev((opacities.m_alpha.lower_bound(value)),1)->second;
        float upper_alpha = opacities.m_alpha.lower_bound(value)->second;

        float alpha_weight = (value - lower_alpha_value)/(upper_alpha_value - lower_alpha_value);

        alpha = (1-alpha_weight) * lower_alpha + alpha_weight * upper_alpha;

    }

    return alpha;
}

#endif // TRANSFERFUNCTION_H
