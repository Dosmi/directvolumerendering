#include "rgba.h"
#include <iostream>

RGBA::RGBA()
{}

RGBA::RGBA(float red, float green, float blue)
    : m_red(red), m_green(green), m_blue(blue), m_alpha(0.f),
      m_index(0.f), m_channel(_RGBA_)
{}

RGBA::RGBA(float red, float green, float blue, float alpha)
    : m_red(red), m_green(green), m_blue(blue), m_alpha(alpha),
      m_index(0.f), m_channel(_RGBA_)
{}

RGBA::RGBA(float red, float green, float blue, float alpha, float indexing_value)
    : m_red(red), m_green(green), m_blue(blue), m_alpha(alpha),
      m_index(indexing_value), m_channel(_RGBA_)
{}

RGBA::RGBA(float indexing_value, float value, channel channel_val)
    :  m_index(indexing_value), m_channel(channel_val)
{
    if(channel_val == RED)
    {
        m_red = value;
        m_green = 0.f;
        m_blue = 0.f;
        m_alpha = 255.f;
    }
    if(channel_val == GREEN)
    {
        m_red = 0.f;
        m_green = value;
        m_blue = 0.f;
        m_alpha = 255.f;
    }
    if(channel_val == BLUE)
    {
        m_red = 0.f;
        m_green = 0.f;
        m_blue = value;
        m_alpha = 255.f;
    }
    if(channel_val == ALPHA)
    {
        m_red = 0.f;
        m_green = 0.f;
        m_blue = 0.f;
        m_alpha = value;
    }
}


void RGBA::print()
{
    std::cout << "R: " << m_red << " G: " << m_green << " B: " << m_blue << " A: " << m_alpha << "\n";
}

std::string RGBA::getRGBAString()
{
    return "RGBA(" + std::to_string(m_red) + "," + std::to_string(m_green)+ ","+
            std::to_string(m_blue)+ "," + std::to_string(m_alpha)+ ")";
}
