#ifndef RGBA_H
#define RGBA_H
#include <cstdint>
#include <string>

enum channel {_RGBA_, RED, GREEN, BLUE, ALPHA, RGB };

class RGBA
{
public:
    RGBA();
    RGBA(float indexing_value, float value, channel channel_val);
    RGBA(float red, float green, float blue);
    RGBA(float red, float green, float blue, float alpha);
    RGBA(float red, float green, float blue, float alpha, float indexing_value);
    void print();
    std::string getRGBAString();

    float m_red;
    float m_green;
    float m_blue;
    float m_alpha;

    float m_index;
    channel m_channel;
};

#endif // RGBA_H
