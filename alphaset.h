#ifndef ALPHASET_H
#define ALPHASET_H

#include <QObject>
#include <QWidget>
#include <QMouseEvent>

#include <QPainter>
#include <QDebug>
#include "gradient.h"
#include "alpha.h"

struct ControlPoint
{   // coordinates in the widget:
    float x;
    float y;
    // data value - colour mapping
    float ct_number;
    float alpha;
};

class AlphaSet : public QWidget
{
    Q_OBJECT
public:
    explicit AlphaSet(QWidget *parent = nullptr);
    void paintEvent( QPaintEvent * ) override;
    void drawSelectorRGB(QPainter* painter, int x, int y, int z, int j);
    void drawLine(QPainter* painter, int x, int y, int z, int j);

    void mouseDoubleClickEvent(QMouseEvent* event) override;
    void mousePressEvent(QMouseEvent* event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;

    RGBA transferalpha(float value, Gradient col_gradient, Alpha opacities);

    Gradient m_col_gradient;
    Alpha    m_col_alphas;

    // implemented one-step undo
    // (remembering just one previous value for undo)
    void undoAlpha();
    Alpha m_undo_col_alphas;

    float m_stretch;
    bool  m_dragging;
    float m_dragged_x;
    float m_dragged_y;

    float m_previous_x;
    float m_original_x;

    float m_prev_nbor;
    float m_next_nbor;

    std::vector<ControlPoint> m_ctrl_pts;


signals:
    void signalGradient(Gradient newGradient);
    void signalAlpha(Alpha newAlpha);

    void signalAddAlphaPoint(Alpha newAlpha);
    void signalRemoveAlphaPoint(float oldAlpha);

    void signalAddAlphaPoint2(Alpha newAlpha);
    void signalRemoveAlphaPoint2(float oldAlpha);

    void signalUndoAlpha(Alpha oldAlpha);

public slots:
    void slotGradient(Gradient newGradient);
    void slotAlpha(Alpha newAlpha);
};

#endif // ALPHASET_H
