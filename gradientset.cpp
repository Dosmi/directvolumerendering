#include "gradientset.h"
#include "gradientpaint.h"
#include <QPainter>
#include <QDebug>
#include <cstdlib>

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <iostream>

GradientSet::GradientSet(QWidget *parent) : QWidget(parent)
{
    m_stretch = 3.f;
    m_active_value = -1.f;
}

// slots for receiving data sent from other widgets:
void GradientSet::slotGradient(Gradient newGradient)
{
    m_ctrl_pts.clear();
    m_col_gradient = newGradient;
    repaint();
}

void GradientSet::slotAlpha(Alpha newAlpha)
{
    m_col_alphas = newAlpha;
    update();
}

void GradientSet::drawSelectorRGB(QPainter *painter, int x, int y, int z, int j){
  painter->setPen(Qt::black);

  painter->setBrush(Qt::white);
  painter->drawEllipse(x-2, y-2, z+4, j+4);

  // if clicked close to the control point button:
  if (approxCompare(m_active_value*m_stretch, x, 0.01f))
  {
      painter->setBrush(Qt::green);
  }
  else painter->setBrush(Qt::black);
  painter->drawEllipse(x, y, z, j);
}

void GradientSet::sendLightInfo(int light_info)
{
    emit(signalLightingInfo(light_info));
}

void GradientSet::sendClampNoiseStart(float clamp_start)
{
    emit(signalClampStart(clamp_start));
}

void GradientSet::sendClampNoiseEnd(float clamp_end)
{
    emit(signalClampEnd(clamp_end));
}

void GradientSet::sendStepSize(float step_size)
{
    emit(signalStepSize(step_size));
}

void GradientSet::sendMaxSteps(float max_steps)
{
    emit(signalMaxSteps(max_steps));
}

void GradientSet::sendColourFile(QString colour_file)
{
    emit(signalColourFile(colour_file));
}

void GradientSet::sendAlphaFile(QString alpha_file)
{
    emit(signalAlphaFile(alpha_file));
}

void GradientSet::sendDataFile(QString data_file)
{
    emit(signalDataFile(data_file));
}

bool GradientSet::approxCompare(float a, float b, float tolerancy)
{
    if(abs(a-b) < tolerancy) return true;
    else return false;
}




void GradientSet::undoColour()
{
    m_col_gradient = m_undo_col_gradient;
    m_ctrl_pts = m_undo_ctrl_pts;
    emit(signalUndoColour(m_undo_col_gradient ));
    repaint();
}

void GradientSet::slotGradientRed(float newGradient)
{
    m_col_gradient.m_colour_gradient.find(float(m_active_value))->second.m_red = newGradient;
}

void GradientSet::paintEvent( QPaintEvent* )
{

  QPainter painter( this );
  // no antialiasing, want thin lines between the cell
  painter.setRenderHint( QPainter::Antialiasing, true );

  // black thin boundary around the cells
  //  QPen pen( Qt::red );
  //  pen.setWidth(0.);

  RGBA rect_slice_col(255.f, 255.f, 255.f);

  int local_stretch = int(m_stretch);
  int red, green, blue;
  for (int i_column = 0 ; i_column < 100*m_stretch; i_column+=local_stretch)
  {
        QRect rect(i_column,m_stretch, m_stretch,100);
        // get the colour components (linear interpolation) of a rectangle slice:
        rect_slice_col = this->transfer(i_column/m_stretch,
                                        m_col_gradient,
                                        m_col_alphas);

        red = int(rect_slice_col.m_red  );
        green = int(rect_slice_col.m_green);
        blue = int(rect_slice_col.m_blue );

        QColor c = QColor(red, green, blue);

        // fill with color for the pixel
        painter.fillRect(rect, QBrush(c));
    }
    m_ctrl_pts.clear(); // use clear, not empty here
    for (int i_column = 0 ; i_column < 100*m_stretch; i_column+=local_stretch)
    {
      if (m_col_gradient.m_colour_gradient.find(i_column/m_stretch) != m_col_gradient.m_colour_gradient.end())
      {
          m_ctrl_pts.push_back({static_cast<unsigned int>(i_column), 50,
                                float(i_column)/m_stretch,
                                m_col_gradient.m_colour_gradient.find(i_column/m_stretch)->second });
          drawSelectorRGB(&painter, i_column, 50, 5,5);
      }
    }


}

void GradientSet::mousePressEvent(QMouseEvent *event)
{
    int x = event->x(), y = event->y();
    bool create_new = true;

    if (y < 100)
    {
        int cp_counter = 0;
        for (ControlPoint1 cp : m_ctrl_pts)
        {
            cp_counter++;

            if ( (abs(int(cp.x - static_cast<unsigned int>(x)) ) < 5)
                 && (abs(int(cp.y - static_cast<unsigned int>(y))) < 5) )
            {
                create_new = false;

                m_active_value = cp.value;
                emit(signalUnlockInput());
                emit(signalActiveValue(cp.value));

                emit(signalSetDialog(cp.value));

                RGBA packet(cp.col.m_red, cp.col.m_green, cp.col.m_blue, 255.f, cp.value);

                // signal the chosen RGB to the spinboxes, ...
                // ... which allow further colour tuning
                emit(signalSetRedSpinbox(packet));
                emit(signalSetGreenSpinbox(packet));
                emit(signalSetBlueSpinbox(packet));

                // code for dragging control-points:
                m_clicked = true;
                m_dragged_y = 100.f - y;
                m_dragged_x = cp.value;

                m_original_x = cp.value;
                m_original_col = cp.col;
                m_original_col.m_index = cp.value;

                m_previous_x = m_original_x;

                m_prev_nbor = std::prev((m_col_gradient.m_colour_gradient.lower_bound(m_original_x)),1)->first;
                m_next_nbor = m_col_gradient.m_colour_gradient.upper_bound(m_original_x)->first;

                if(event->button() == Qt::RightButton)
                {
                    m_ctrl_pts.erase(m_ctrl_pts.begin()+cp_counter);
                    emit(signalRemovePoint(float(cp.x)/m_stretch));
                    m_active_value = -1.f;
                    emit(signalActiveValue(m_active_value));
                    emit(signalSetDialog(m_active_value));
                    RGBA packet(0.f, 0.f, 0.f, 255.f, 0.f);

                    emit(signalSetRedSpinbox(packet));
                    emit(signalSetGreenSpinbox(packet));
                    emit(signalSetBlueSpinbox(packet));

                    emit(signalLockInput());
                }
                create_new = false;
            }

        }

        if ((create_new) // if new control point is flagged to be created, and it does not exist yet:
            && (m_col_gradient.m_colour_gradient.find(truncf(float(x)/m_stretch)) == m_col_gradient.m_colour_gradient.end()))
        {
            RGBA rect_slice_col = this->transfer(truncf(float(x)/m_stretch),
                                                 m_col_gradient,
                                                 m_col_alphas);
            rect_slice_col.m_index = x/m_stretch;

            m_undo_col_gradient = m_col_gradient;
            m_undo_ctrl_pts = m_ctrl_pts;
            emit(signalAddNewPoint(rect_slice_col));
        }
    }
}

/*
void GradientSet::mouseMoveEvent(QMouseEvent *event)
{
    if (m_clicked) m_dragging = true;
    else m_clicked = false;
    int x = event->x(), y = event->y();

    qDebug() << "dragging ... " << x << " " << y;


//    if( (m_dragging) && (x > m_prev_nbor) && (x < m_next_nbor) )
    if( (m_dragging) && (x > m_prev_nbor * m_stretch) && (x < m_next_nbor * m_stretch)
        && (y > 0) && (y<100) && (abs(m_previous_x-x) > 2))
    {
//        qDebug() << "prev: " << m_previous_x << "orig: " << m_original_x;
//        if(m_previous_x != m_original_x)
//        {
//            m_previous_x = m_dragged_x;
//            qDebug() << "prev: " << m_previous_x;
//        }
        m_previous_x = m_dragged_x;

        m_dragged_y = 100.f - y;
        m_dragged_x = x/m_stretch;

        qDebug() << "added: " << m_dragged_x;

//        RGBA orig_col = m_col_gradient.m_colour_gradient.find(m_original_x)->second;
//        orig_col.m_index = (float)m_dragged_x;

        m_col_gradient.addPoint((float)m_dragged_x, m_original_col.m_red, m_original_col.m_green, m_original_col.m_blue );
        m_col_gradient.removePoint(m_previous_x);

        emit(signalAddNewPoint2(m_original_col));
        emit(signalRemovePoint2(m_previous_x));

//        emit(signalRemoveAlphaPoint2(m_original_x));
//        emit(signalAddAlphaPoint2(m_col_alphas));
    }
    repaint();
}

void GradientSet::mouseReleaseEvent(QMouseEvent *event)
{
    qDebug() << "stopped at: " << event->x() << ", " << event->y();

    if(m_dragging)
    {
        m_dragging = false;
//        emit()
//        m_col_alphas.addAlphaPoint(m_dragged_x, m_dragged_y);
//        m_col_alphas.removeAlphaPoint(m_original_x);
        RGBA orig_col = m_col_gradient.m_colour_gradient.find(m_original_x)->second;
        m_col_gradient.addPoint((float)m_dragged_x, orig_col.m_red, orig_col.m_green, orig_col.m_blue );

//        m_col_gradient.m_colour_gradient = m_dragged_x;
//        m_col_alphas.m_y = m_dragged_y;
        emit(signalAddNewPoint(orig_col));
        emit(signalRemovePoint(m_previous_x));
//        emit(signalAddAlphaPoint(m_col_alphas));
//        emit(signalRemoveAlphaPoint(m_original_x));
    }
    repaint();
}
*/
// the transfer function - takes in a value, gets the colour and alpha value:
RGBA GradientSet::transfer(float value, Gradient col_gradient, Alpha opacities)
{
    float lerp_red, lerp_green, lerp_blue, lerp_alpha;

    // there are two disjoint arrays:
    // ... col_gradient - containing the control points for the RGB colours, ...
    // ... opacities    - containing the control points for the opacities

    // here, for a given value, we find two neighbours to linearly interpolate between ...
    // ... (for colour and opacity values):
    float lower_col_value = std::prev((col_gradient.m_colour_gradient.lower_bound(value)),1)->first;
    float upper_col_value = col_gradient.m_colour_gradient.lower_bound(value)->first;

    float lower_alpha_value = std::prev((opacities.m_alpha.lower_bound(value)),1)->first;
    float upper_alpha_value = opacities.m_alpha.lower_bound(value)->first;

    RGBA lower_nbor_colour    = std::prev((col_gradient.m_colour_gradient.lower_bound(value)),1)->second;
    RGBA upper_nbor_colour    = col_gradient.m_colour_gradient.lower_bound(value)->second;
    float lower_nbor_alpha    = std::prev((opacities.m_alpha.lower_bound(value)),1)->second;
    float upper_nbor_alpha    = opacities.m_alpha.lower_bound(value)->second;

    float col_weight   = (value - lower_col_value)/(upper_col_value - lower_col_value);
    float alpha_weight = (value - lower_alpha_value)/(upper_alpha_value - lower_alpha_value);

    lerp_red   = (1-col_weight) * lower_nbor_colour.m_red   + col_weight * upper_nbor_colour.m_red;
    lerp_green = (1-col_weight) * lower_nbor_colour.m_green + col_weight * upper_nbor_colour.m_green;
    lerp_blue  = (1-col_weight) * lower_nbor_colour.m_blue  + col_weight * upper_nbor_colour.m_blue;

    lerp_alpha = (1-alpha_weight) * lower_nbor_alpha + alpha_weight * upper_nbor_alpha;

    // return the full colour, given in RGBA ...
    // ... (with given 'alpha' being in %, hence division by 100 and multiplication by 255)
    return RGBA(lerp_red, lerp_green, lerp_blue, (lerp_alpha/100)*255);
}

