#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QTimer>

#include "gradient.h"
#include "alpha.h"
#include "gradientpaint.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

// -------------------------------- inherited from popup --------------------------------:
    void connectToPainter();

    Gradient m_col_gradient;
    Alpha    m_col_alphas;

    GradientPaint* painter;

    float m_active_value;


    // new:
    // spinbox RGB
    RGBA m_spinbox_col;

    // colour to send for update:
    RGBA m_col_packet;

    QTimer* pTimer;

    Ui::MainWindow *ui;
//private:
//    Ui::ColourGradientDialog *ui;

signals:
    void signalGradient(Gradient newGradient);
    void signalGradientTo(float newGradient);
    void signalAlpha(Alpha newAlpha);

    void signalColourUpdatePacket(RGBA);

private slots:
    void on_red_dbl_spinbox_valueChanged(double value);
    void on_green_dbl_spinbox_valueChanged(double value);
    void on_blue_dbl_spinbox_valueChanged(double value);
    void on_set_colour_clicked();

    void slotGradient(Gradient newGradient);
    void slotAlpha(Alpha newAlpha);

    void slotActiveValue(float newActiveValue);
    void slotSetDialog(float newDialogValue);
    void slotSetRedSpinbox(RGBA newRedValue);
    void slotSetGreenSpinbox(RGBA newGreenValue);
    void slotSetBlueSpinbox(RGBA newBlueValue);

    void slotLockInput();
    void slotUnlockInput();

    void updateFPS(int fps);
    void updateMS(int ms);

    void slotOptimalStep(double optimal_step);
    void slotOptimalMaxSteps(double optimal_max_steps);

//    void slotAddNewPoint(RGBA newPoint);
// -------------------------------- inherited from popup --------------------------------


    void on_undo_alpha_clicked();
    void on_undo_colour_clicked();
    void on_specular_light_cbox_stateChanged(int arg1);
    void on_diffuse_light_cbox_stateChanged(int arg1);
    void on_ambient_light_cbox_stateChanged(int arg1);
    void on_clamp_noise_start_valueChanged(double arg1);
    void on_clamp_noise_end_valueChanged(double arg1);

    void on_sbox_step_size_valueChanged(double arg1);
    void on_sbox_max_steps_valueChanged(int arg1);

    void on_load_colours_clicked();
    void on_load_alphas_clicked();
    void on_btn_update_colours_clicked();
    void on_toggle_ess_stateChanged(int arg1);
    void on_toggle_updateBB_stateChanged(int arg1);
    void on_toggle_precompute_stateChanged(int arg1);
    void on_toggle_lerp_stateChanged(int arg1);
    void on_toggle_log_stateChanged(int arg1);
    void on_button_change_data_clicked();
    void on_comboBox_currentIndexChanged(int index);
};
#endif // MAINWINDOW_H
