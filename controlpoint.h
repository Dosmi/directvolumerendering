#ifndef CONTROLPOINT_H
#define CONTROLPOINT_H
#include "rgba.h"

struct ControlPoint
{
    unsigned int x;
    unsigned int y;
    float value;
    RGBA col;
};


#endif // CONTROLPOINT_H
